import matplotlib.pyplot as plt
x = [0, .1, .18, .27, .35, .44, .51, .59, .67, .74, .81, .88, .95, 1.02]
y = [0, .16, .29, .39, .48, .54, .59,.61, .62,  .61, .58, .53, .47, .40]
dt = .1
m = 57e-3

def delta_v(x, y, dt, i, echelle):
    #vecteur vitesse au point suivant
    vx2 = (x[i+2] - x[i]) / (2*dt)
    vy2 = (y[i+2] - y[i]) / (2*dt)
    #vecteur vitesse au point précédent
    vx1 = (x[i] - x[i-2]) / (2*dt)
    vy1 = (y[i] - y[i-2]) / (2*dt)
    # vecteur delta_v
    dvx = vx2 - vx1
    dvy = vy2 - vy1
    #tracé des vecteurs
    plt.quiver(x[i-1], y[i-1], vx1, vy1, scale=echelle, color='green', angles='xy', scale_units='xy')
    plt.quiver(x[i+1], y[i+1], vx2, vy2, scale=echelle, color='green', angles='xy', scale_units='xy')
    plt.quiver(x[i], y[i], dvx, dvy, scale=echelle, color='red', angles='xy', scale_units='xy')

# figure
plt.plot(x,y, 'o')
plt.xlim(0, 1.2*max(x))
plt.ylim(0, 1.2*max(y))
plt.xlabel('x / m')
plt.ylabel('y / m')
plt.grid()
delta_v(x, y, dt, 4, 4)
plt.pause(2)
delta_v(x, y, dt, 6, 4)
plt.pause(2)
delta_v(x, y, dt, 10, 4)
plt.pause(1)
plt.show()
