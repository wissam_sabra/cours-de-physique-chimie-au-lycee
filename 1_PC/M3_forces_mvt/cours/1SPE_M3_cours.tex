\documentclass[french]{article}

\input{../../../preambules/mes_packages_lua}
\input{../../../preambules/style_cours}
\fancyhead[L]{\textbf{M3.} \textcolor{gray!70}{Première spécialité}}
\fancyhead[R]{\themeMvt}
% -------------------------------------------------------------------------------
\begin{document}
\titre{\textbf{Chapitre 9.} Mouvement et forces}

Dans le cadre de ce cours, le système étudié est toujours \emph{un point matériel}

\section{Vitesse et variation de vitesse}
\subsection{Vecteur vitesse}
\begin{aConnaitre}
  \begin{listeAC}
    \item Si on décompose la trajectoire d'un point en une succession de 
    positions à intervalles de temps réguliers $ \tau $ : $ M_0 $, $ M_1 $, 
    $ M_2 $, ..., $ M_i $, $ M_{i+1} $, on peut \emph{approcher} le 
    \textbf{vecteur vitesse} $ \vv{v_i} $ au point $ M_i $, en encadrant $ M_i 
    $ par les points $ M_{i-1} $ et $ M_{i+1} $.
    \item \textcolor{red}{ La meilleure approximation du vecteur vitesse en un 
      point est : }
  \end{listeAC}
  % 
  \begin{center}
    \cadre{\large
      \textcolor{red!85!black}{$ \vv{v_i} = \dfrac{\vv{M_{i-1} M_{i+1}}}{t_{i+1} -
          t_{i-1}} $}}
  \end{center}
  ici $ t_{i+1} - t_{i-1} = 2 \tau $
\end{aConnaitre}
%
\begin{figure}[ht]
  \begin{minipage}{.55\linewidth}
  \centering
  \input{./fig/vect_deplac_vit.tex}
  \caption{vecteurs déplacement et vecteur vitesse}
\end{minipage}
\begin{minipage}{.4\linewidth}
  \underline{Caractéristiques de $ \vv{v} $ }:
  \begin{itemize}[leftmargin=*]
    \item \textbf{Point d'application} : $ M_i $,
    \item \textbf{Direction} : tangente à la trajectoire,
    \item \textbf{Sens }: celui du mouvement,
    \item \textbf{Valeur} : celle de la vitesse en \unit{\m\per\s}.
  \end{itemize}
\end{minipage}
\end{figure}
%
\subsection{Variation du vecteur vitesse }
\begin{figure}[ht]
  \begin{minipage}{.45\linewidth}
    Lors d’un mouvement, le vecteur vitesse d’un système peut varier \emph{en direction, en
      sens ou en valeur}. Pour traduire cette variation, on peut définir le vecteur
    variation de vitesse en un point.
  \end{minipage}
  %
  \begin{minipage}{.45\linewidth}
    \centering
    \input{ ./fig/traject_vect_vit.tex }
    \caption{vecteurs vitesse qui varie}
  \end{minipage}
\end{figure}
\begin{aConnaitre} Le \textbf{vecteur variation de vitesse} $ \Delta\vv{v} $ d'un système
  en mouvement entre les positions $ M_{i-1} $ et $ M_{i+1} $ est défini par la relation :
  
  \begin{center} \cadre{\large \textcolor{red!85!black}{$ \Delta \vv{v} = \vv{v_{i+1}} -
        \vv{v_{i-1}} $}}
  \end{center}
\end{aConnaitre}
%
Construisons $ \Delta \vv{v_2} $ au point $ M_2 $ sachant
\[
  \Delta \vv{v_2} = \vv{v_3} - \vv{v_1}
\]

\begin{figure}[ht]
\begin{minipage}{.55\linewidth}
  \input{ ./fig/construction_delta_v.tex }
  \caption{Vecteur variation de vitesse }
\end{minipage}
\begin{minipage}{.45\linewidth}
  \begin{grandCadre}
    \textbf{Construction :}
    \begin{dingautolist}{202}
      \item On a les vecteurs vitesses en $ M_1 $ et $ M_3 $.
      \item On reporte $ \vv{v_3} $ en partant de $ M_2 $.
      \item On reporte $ - \vv{v_1} $ à l'extrémité de $ \vv{v_3} $.
      \item On trace $ \Delta \vv{v_2}  = \vv{v_3} - \vv{v_1} $.
    \end{dingautolist}
  \end{grandCadre}
\end{minipage}
\end{figure}
%
\textbf{\underline{L'orientation du vecteur variation de vitesse :}}

\begin{center}
\input{ ./fig/orientation_delta_v.tex }
\end{center}
%
\begin{center}
  \begin{tabular}{|*{3}{>{\centering\arraybackslash}m{5.2cm}|}}
    \hline 
    \rowcolor{gray!40} \ding{202} Mouvement ralenti 
    & \ding{203} Mouvement accéléré 
    & \ding{204} Mouvement uniforme 
    \\\hline
    $\Delta \vv{v}$ dans le sens opposé au mouvement 
    & $\Delta \vv{v}$ dans le sens du mouvement 
    & $\Delta \vv{v} = \vv{0}$ 
    \\\hline
  \end{tabular}
\end{center}
%
\section{Somme des forces appliquées à un système}
\subsection{Principe d'inertie (rappels)}
Si les forces qui s’appliquent sur un système se compensent, ou s’il n’est soumis à aucune
force, alors il est immobile ou en mouvement rectiligne uniforme et réciproquement.
% 
\begin{aConnaitre}
  D'après le principe d'inertie, si la \textbf{variation du vecteur vitesse 
    est nulle} alors la \textbf{somme vectorielle des forces qui s'exercent sur 
    le système est nulle}. La réciproque est vraie \newline
  \begin{center}
    \cadre{
      \textcolor{red!85!black}{ $ \vv{v} = \vv{cste} 
        \iff \sum \vv{F} = \vv{0} $}
    }  
  \end{center}
\end{aConnaitre}
% 
\begin{ptMath}
  Si le vecteur vitesse est constant au cours du temps, le vecteur variation 
  de vitesse est égal au vecteur nul : 
  \textcolor{red!85!black}{$ \vv{v} = \vv{cste} \iff \vv{\Delta v} = \vv{0} $}
\end{ptMath}
%
\subsection{Déterminer la somme vectorielle des forces}
\begin{aConnaitre}
  Un système soumis à plusieurs forces se comporte comme s’il n’était soumis qu’à une
  seule force correspondant à leur somme vectorielle, également appelée \textbf{résultante
    des forces}.
\end{aConnaitre}

\begin{figure}[ht]
  \centering
  \begin{minipage}{.5\linewidth}
    \input{ ./fig/somme_forces.tex }
    \caption{Construction du vecteur somme de forces}
  \end{minipage}
  %
  \begin{minipage}{.4\linewidth}
    On calcule les coordonnées de la somme des forces en ajoutant les différentes
    coordonnées de chacun des vecteurs forces :

    \definecolor{colA}{rgb}{0, .55, .45}
    \definecolor{colB}{rgb}{0, 0, .45}
    \newcommand{\Fa}{\textcolor{colA}{\vv{F}{\text{A}}}}
    \newcommand{\Fb}{\textcolor{blue}{\vv{F}{\text{B}}}}
    \newcommand{\Fbx}{\textcolor{colB}{F_{\text{B}x}}}
    \newcommand{\Fby}{\textcolor{colB}{F_{\text{B}y}}}
    \newcommand{\Fax}{\textcolor{colA}{F_{\text{A}x}}}
    \newcommand{\Fay}{\textcolor{colA}{F_{\text{A}y}}}
    \begin{align*}
      \textcolor{orange}{\sum \vv{F}}
      &= \Fa + \Fb \\
      &=
        \begin{pmatrix} \Fax \\ \Fay \end{pmatrix}
        +
        \begin{pmatrix} \Fbx \\ \Fby \end{pmatrix} \\
      &=
        \begin{pmatrix} \Fax + \Fbx \\ \Fay + \Fby  \end{pmatrix} \\
    \end{align*}
  \end{minipage}
\end{figure}
%
\subsection{Projection d'un vecteur}
\begin{aConnaitre}
  En Physique, on exprime les coordonnées des vecteurs force en fonction de leur norme.
\end{aConnaitre}

% \enlargethispage{1.5cm}
\begin{figure}[ht]
  \centering
  \begin{minipage}{.45\linewidth}
    \begin{center}
      \input{ ./fig/project_force.tex }
      \caption{projection de vecteurs force}
    \end{center}
  \end{minipage}
  \begin{minipage}{.45\linewidth}
    \centering
    \begin{tblr}{rowspec ={XX},rows = {ht=1cm}}
      $ \textcolor{red}{\vv{F_1} = \begin{pmatrix} F_{1} \cdot cos(\alpha) \\ F_{1} \cdot
          sin(\alpha) \end{pmatrix} }$ &
      $ \textcolor{green!50!black}{\vv{F_2} = \begin{pmatrix} 0 \\ F_{2} \end{pmatrix}} $
      \\
      $ \textcolor{orange}{\vv{F_3} = \begin{pmatrix} - F_{3} \cdot cos(\beta) \\ F_{3}
                                        \cdot sin(\beta) \end{pmatrix} }$
      &
      $ \textcolor{blue!70!green}{\vv{F_4} = \begin{pmatrix} - F_{4} \\ 0 \end{pmatrix}} $
      \end{tblr}
  \end{minipage}
\end{figure}
% -------------------------------------------------------------------------------
\section{Relation entre forces et variation du vecteur vitesse}
\subsection{Expression approchée de la deuxième loi de Newton}
\begin{aConnaitre}
  La relation approchée existant entre la somme des forces $ \sum \vv{F} $ exercées sur un
  système de masse $ m $ et son vecteur variation de vitesse $ \Delta \vv{v}(t) $ entre la
  date $ t $ et la date $ t + \Delta t $ est :
  
  \formule{\large $ \sum \vv{F} = m~ \dfrac{\Delta \vv{v}}{\Delta t}$}
  {$ \sum F $ en \unit{\newton} \\
    $ m $ en \unit{\kg} \\
    $ \Delta v $ en \unit{\m\per\s} \\
    $ \Delta t $ en \unit{\s}
  }

  La variation du vecteur vitesse et la somme des forces appliquées à ce système ont donc
  \textbf{la même direction et le même sens.}
\end{aConnaitre}

\begin{minipage}{.15\linewidth}
  \includegraphics[scale=.2]{ ./img/newton.png }
\end{minipage}
\begin{minipage}{.8\linewidth}
  On nommera par la suite cette relation \textbf{deuxième loi de Newton}. Elle n'est
  valable que dans certains référentiels qualifiés de \textbf{galiléens.}.
\end{minipage}
%
\subsection{Cas particulier : la chute libre}
Par définition, lors d’une chute libre, un corps n’est soumis qu’à son poids. En appliquant
la deuxième loi de Newton, on peut écrire :

\renewcommand{\CancelColor}{\color{red}}
\begin{align*}
  \vv{P} &= m \times \dfrac{\Delta \vv{v}}{\Delta t} \\
  \cancel{m} \times \vv{g} &= \cancel{m} \times \dfrac{\Delta \vv{v}}{\Delta t} \\
  \vv{g} &= \dfrac{\Delta \vv{v}}{\Delta t}
\end{align*}

\begin{aConnaitre}
  Lors d'une chute libre, la variation de la vitesse ne dépend pas de la masse du système.
\end{aConnaitre}

% \begin{tikzpicture}
%   \begin{axis}
		
%     \addplot table [x={x},y={y}] {
%       t			x	                y
%       0.000000000E0		2.397003745E-1		2.771535581E-1
%       6.666700000E-2		3.932584270E-1		5.730337079E-1
%       1.333340000E-1		5.505617978E-1		8.089887640E-1
%       2.000010000E-1		7.003745318E-1		1.014981273E0
%       2.666680000E-1		8.501872659E-1		1.164794007E0
%       3.333350000E-1		1.000000000E0		1.269662921E0
%       4.000020000E-1		1.149812734E0		1.337078652E0
%       4.666690000E-1		1.284644195E0		1.359550562E0
%       5.333360000E-1		1.445692884E0		1.340823970E0
%       6.000030000E-1		1.599250936E0		1.273408240E0
%       6.666700000E-1		1.741573034E0		1.176029963E0
%       7.333370000E-1		1.910112360E0		1.007490637E0
%       8.000040000E-1		2.041198502E0		8.314606742E-1
%       8.666710000E-1		2.202247191E0		5.767790262E-1
%       9.333380000E-1		2.344569288E0		3.071161049E-1
%     };
%   \end{axis}
% \end{tikzpicture}
%
\subsection{Influence de la masse du système} D'après la relation approchée $ \sum \vv{F}
= m \times \dfrac{\Delta \vv{v}}{\Delta t} $ : plus la masse d'un système est grande, plus il
est difficile de modifier le mouvement de ce système.

\begin{minipage}{.5\linewidth}
  \begin{itemize}[leftmargin=*]
    \item Afin d'obtenir la \textcolor{green!85!black}{même variation de vitesse
      $ \Delta \vv{v} $} pour deux systèmes de masses différentes, il faut exercer sur le
    système de plus grande masse une somme des forces de plus grande valeur.
    
    \item Si on exerce la \textcolor{blue}{même somme des forces $ \sum \vv{F} $} sur deux
    systèmes de masses différentes, plus la masse du système est grande, plus la valeur de
    son vecteur variation de vitesse est petite.
  \end{itemize}
  %
\end{minipage} 
\begin{minipage}{.4\linewidth}
  \centering \includegraphics[scale=.18]{ ./img/masseSysteme.png }
\end{minipage}
\end{document}
