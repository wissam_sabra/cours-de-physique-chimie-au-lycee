\documentclass[french]{article}
\input{ ../../../preambules/mes_packages_lua.tex }
\input{ ../../../preambules/style_cours.tex }
\fancyhead[L]{\textbf{E1.} \textcolor{gray!70}{Première spécialité}}
\fancyhead[R]{\themeEnergie}
% -------------------------------------------------------------------------------
\begin{document}
\titre{\textbf{Chapitre 11.} Conversion de l'énergie stockée dans la matière organique.}
% -------------------------------------------------------------------------------
\section{Combustion complète}
\begin{aConnaitre}
  \begin{listeAC}
    \item Une combustion est une réaction \emph{d'oxydoréduction} entre un
    \emph{combustible} et un \emph{comburant} (généralement le dioxygène).
    \item La combustion d'un alcane ou d'un alcool est \emph{complète} quand il ne se
    forme que du dioxyde de carbone et de l'eau.
    \item La combustion est une réaction \emph{exothermique}.
  \end{listeAC}
\end{aConnaitre}
%
\begin{ex}{Combustion complète du propane:}

  \ch{C3H8(g) + 5 O2(g) -> 3 CO2(g) + 4 H2O(g)}
\end{ex}
%
\begin{ex}{Combustion complète du méthanol}

  \ch{2 CH4O(g) + 3 O2(g) -> 2 CO2(g) + 4 H2O(g)}
\end{ex}
% -------------------------------------------------------------------------------
\section{Aspect énergétique des combustions}
\subsection{Énergie molaire de liaison}
\begin{aConnaitre}
  \begin{listeAC}
    \item L'énergie molaire de liaison dans une molécule diatomique est l'énergie qu'il
    faut fournir pour \emph{rompre} les liaisons d'une mole de molécules.
    \begin{center}
      \ch{AB(g) -> A(g) + B(g)}
    \end{center}
    %
    \item L'énergie de liaison est toujours \emph{positive}
    \item Dans une molécule, l'énergie molaire d'une liaison est l'énergie nécessaire à la 
    rupture de cette liaison dans une mole de molécule.
    \item \emph{L'énergie molaire de liaison d'une molécule polyatomique} est la
    \emph{somme} des énergies molaires de liaisons de toutes les liaisons.
  \end{listeAC}
\end{aConnaitre}
%
% \newcommand{\el}{E_{\text{l}}}
\newcommand{\Eli}[1]{E_{\text{l}}(\ch{#1})}
\begin{ex}{Énergie molaire de liaison de l'éthanol}
  \begin{align*}
   \Eli &= \Eli{C-C} + 5\Eli{C-H} + \Eli{C-O} + \Eli{O-H} \\
       &=  348 + 5 \times 415 + 350 + 463 \\
       &= \qty{3,24e3}{\kilo\joule\per\mole}
  \end{align*}
\end{ex}
%
\begin{figure}[ht]
  \centering
  \chemfig{H-C(-[2]H)(-[-2]H) - C(-[2]H)(-[-2]H) - \charge{90=\|, -90=\|}{O} - H}
  \caption{Représentation de Lewis de l'éthanol}
\end{figure}
\subsection{Énergie de réaction}
\begin{aConnaitre}
  \begin{listeAC}
    \item \emph{l'énergie molaire de réaction} asociée à une transformation chimique en
    phase gazeuse peut être approchée en faisant le calcul suivant  :
    
    % (somme des énergies de liaisons des réactifs) - (somme des énergies de liaison
    % des produits)
    \begin{center}
      \cadre{$\Delta E_{\text{r}} = \sum \Eli(réactifs) - \sum \Eli(produits)$}
    \end{center}
    \item Pour une combustion, l'énergie molaire de réaction est l'énergie correspondant à
    la combustion d'une mole de combustible. \emph{Elle est toujours \textbf{négative} car la
      combustion est une réaction exothermique.}
  \end{listeAC}
\end{aConnaitre}
%
% \newcommand{\Eli}[1]{E_{\text{l}}(\ch{#1})}
%
\begin{ex}{Combustion complète du méthane}
\end{ex}
\begin{figure}[ht]
  \centering
  \schemestart
  \chemfig{H - C(-[2]H)(-[-2]H) - H}
  \hspace{.5cm}
  +
  \hspace{.5cm}
  \textcolor{red}{2} \hspace{.2cm} \chemfig{\charge{135=\|, -135=\|}{O}=\charge{45=\|, -45=\|}{O}}
  \hspace{.5cm}
  \arrow
  \hspace{.5cm}
  \chemfig{\charge{135=\|, -135=\|}{O} = C = \charge{45=\|, -45=\|}{O}}
  \hspace{.5cm}
  +
  \hspace{.5cm}
  \textcolor{red}{2} \hspace{.1cm} \chemfig{H - \charge{90=\|, -90=\|}{O} - H}
  \schemestop
  \caption{Réaction modélisant la combustion du méthane}
\end{figure}
%
 \begin{align*}
   \Delta E_{\text{r}}
   &= [ \Eli{CH4} +\textcolor{red}{2} \Eli{O2} ] -
     [ \Eli{CO2} + \textcolor{red}{2} \Eli{H2O} ] \\
   &= [ 4 \times \Eli{C-H} + \textcolor{red}{2} \times \Eli{O=O}] -
     [2 \times \Eli{C=O} + \textcolor{red}{2} \times 2 \Eli{O-H}] \\
   &=  4 \Eli{C-H} + 2 \Eli{O=O} - 2 \Eli{C=O} - 4 \Eli{O-H} \\
   &= 4 \times 415 + 2 \times 498 - 2 \times 804 - 4 \times 463 \\
   &= \qty{-804}{\kilo\joule\per\mole}
 \end{align*}
 % 
 \attention{Pour calculer l'énergie de combustion, on doit écrire l'équation de la réaction
  pour \emph{une mole de combustible.}}
%
\begin{aConnaitre}
  \begin{listeAC}
    \item L'énergie $Q$ mise en jeux lors de la combustion complète d'une quantité de
    matière $n$ de combustible est :
    \formule{$Q = n \times \Delta E_{\text{r}} $}{
      $Q$ / \si{ \joule} \\
      $n$ / \si{\mole} \\
      $\Delta E_\text{r}$ / \si{\joule\per\mol} }
    \item $Q$ est toujours négative pour une réaction de combustion.
    \item L'énergie cédée par transfert thermique est parfois notée $\Delta E$
  \end{listeAC}
\end{aConnaitre}
\subsection{Pouvoir calorifique}
\begin{aConnaitre}
  \begin{listeAC}
    \item Le \emph{pouvoir calorifique} PC d'un combustible est \emph{l'énergie} qu'un
    kilograme de combustible cède à l'environnement lors de sa \emph{combustion complète}.
    \item On peut calculer l'énergie produite lors de la combustion d'une masse $m$ à
    l'aide de la relation suivante:
    \formule{$PC = -\dfrac{Q}{m}$}{
      $Q$ / \si{\joule} \\
      $m$ / \si{\kg} \\
      $PC$ / \si{\joule\per\kg}
      }
      \item Par convention, le pouvoir calorifique est positif.
  \end{listeAC}
\end{aConnaitre}
%
\noindent\textbf{Remarque :} la \emph{tonne d'équivalent pétrole} (tep) est une unité de mesure
    d'énergie. Le pouvoir calorifique du pétrole valant en moyenne
    \SI{42}{\mega\joule\per\kg}, $\SI{1}{tep} = \SI{42}{\giga\joule}$
%
\begin{ex}{}
  La combustion de \SI{3.0}{\kg} de charbon libère une énergie $E = \SI{84}{\mega\joule}$.
  Son pouvoir calorifique vaut donc:
  \[
    PC = \dfrac{E}{m} = \SI{28}{\mega\joule\per\kg}
  \]
\end{ex}
% -------------------------------------------------------------------------------
\section{Enjeux de société}
\subsection{Risques associés aux combustions}
\begin{aConnaitre}
  \begin{listeAC}
    \item C'est une réaction exothermique pouvant provoquer des brûlures.
    \item Si l'apport de dioxygène est insuffisant, la combustion est incompète et produit
    du \emph{monoxyde de carbone CO}, (gaz toxique, inodore et incolore) ainsi que du
    carbone solide sous forme de \emph{particules fines}.
  \end{listeAC}
\end{aConnaitre}
%
\subsection{Axes d'études actuels}
Pour éviter de puiser dans les ressources fossiles on cherche des alternatives
renouvelables:
\begin{itemize}
  \item Utilisation d'agrocarburant, dont l'inconvénient est de mobiliser  des terres au
  détriment de la production de nourriture, et pousse même à la déforestation.
  \item Utilisation de la \emph{biomasse} en brûlant des déchets végétaux.
  \item Utilisation de \emph{microalgues} spécialisées dans la procution de matière combustible.
\end{itemize}
\begin{aConnaitre}
  \begin{listeAC}
    \item La recherche de nouveaux combustibles renouvelables est importantes, 
    \item Le développement de nouvelles technologies, et leur utilisation massive, a comme
    effet l'augmentation des besoins énergétiques et \textbf{une prise de conscience
      collective sur la consommatoin d'énergie semble indispensable}.
  \end{listeAC}
\end{aConnaitre}
\begin{ex}{Les voitures}
  Les rendements des moteurs de voitures actuelles sont bien supérieurs aux premières
  voitures commercialisées, mais leur utilisations de plus en plus massive a pour
  conséquence une augmentation de la production de déchets liés à la combustion de
  carburants.
\end{ex}
%
\begin{ex}{consommation mondiale d'énergie par zone géographique.}
\end{ex}
% \begin{figure}[ht]
%   \centering
%   \begin{tikzpicture}[scale=.7]
%     \begin{axis}[
%       grid=major,
%       y tick label style={/pgf/number format/.cd,
%           set thousands separator={},
%           fixed},
%       x tick label style={/pgf/number format/.cd ,
%           set decimal separator={,},
%           set thousands separator={ },
%           fixed}%      
%         ]
%       \addplot[only marks] coordinates {
%         (1990,    351)
%         (1991,    377)
%         (1995,    399)
%         (1998,    425)
%         (2000,    440)
%         (2005,    483)
%         (2008,    494)
%         (2012,    497)
%         (2016,    492)
%         };
%     \end{axis}
%   \end{tikzpicture}
%   \caption{Evolution de la consommation d'énergie en France}
% \end{figure}
%
\begin{figure}[ht]
  \centering
  \includegraphics[scale=.4]{graphe_energie}
  \raggedleft{\emph{\textit{Source: www.statistiques.developpement-durable.gouv.fr}}}
\end{figure}

\end{document}
