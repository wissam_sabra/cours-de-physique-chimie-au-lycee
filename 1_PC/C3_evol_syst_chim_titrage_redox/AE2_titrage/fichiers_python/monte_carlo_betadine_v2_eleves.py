# ---------------------------------------------------
# Bibliothèques
import matplotlib.pyplot as plt
import numpy as np
# ---------------------------------------------------

####################################################
# Réaction Support:                                #
#             aA + bB  --> cC + dD                 #
#                                                  #
# Réactif titré   :   A                            #
# Réactif titrant :   B                            #
####################################################

### Définition des fonctions ###
def Tiragealéatoire(L):
    tirage=np.random.normal()
    return L[0]+L[1]*tirage
    '''Renvoie  une valeur aléatoire comprise entre X-u(X) et X+u(X)
    en suivant une loie normale

    Argument :
    liste -- liste [X, u(X]
    '''
#
### Données et valeurs expérimentales ###
C_B=[..., ...]       # A Compléter [mesure, incertitude]
V_A=[..., ...]
V_E=[..., ...]

### méthode Monte Carlo pour le titrage de la bétadine ###

# coefficients stoechiométriques
a = 1
b = 2
CA = []  # Création de la liste
iteration = 100000
#
# calcul de cA pour chaque itération
for i in range(iteration):
    CA.append(a / b * Tiragealéatoire(C_B) * Tiragealéatoire(V_E) / Tiragealéatoire(V_A))
#
# la meilleure valeure est la moyenne
moy_CA = sum(CA) / iteration
# Calcul de l'incertitude (relation qui n'est pas à connaitre)
u_CA = (1/(iteration-1) * sum([(val - moy_CA)**2 for val in CA]))**0.5
# Affichage
print('Concentration CA :', moy_CA,' mol/L')
print('Incertitude u_CA :', u_CA,' mol/L')
#
# Calcul de la concentration en masse
...
...
# Affichage
print('Concentration C_m :', Cm,' g/L')
print('Incertitude u_Cm :', u_Cm,' g/L')

#
# Calcul de la masse de polyvidone iodée dans 100 mL
...
...
#
#Comparaison avec la valeur de référence
if ...0 :
    print('La valeur trouvée est conforme avec la valeur de référence')
else :
    print('La valeur trouvée n\'est pas conforme avec la valeur de référence')

# Tracé de l'histogramme
plt.hist(CA, rwidth=1, bins=50, edgecolor="black")
plt.ylabel("Effectif")
plt.xlabel("cA (en mol.L\u207B\u00B9)")
# plt.xticks(rotation=45)
plt.ticklabel_format(style='sci', axis='x',scilimits=(0,0))
plt.show()