import matplotlib.pyplot as plt
from scipy.stats import linregress

c = [4e-5, 6e-5, 8e-5, 10e-5, 40e-5, 50e-5 ]
A = [.14, .16, .16, .33, .59, .70 ]

p = linregress(c,A)
print(p)
A_mod = [p[0]*i + p[1] for i in c]


plt.figure()
plt.plot(c, A, "o:")
plt.plot(c, A_mod, "-")
plt.xlim(0)
plt.ylim(0)
plt.title("titre")
plt.xlabel("c en mol/L")
plt.ylabel("A")
plt.grid()
plt.show()
