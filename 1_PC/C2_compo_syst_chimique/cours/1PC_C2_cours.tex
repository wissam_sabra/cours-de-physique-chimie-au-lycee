\documentclass[french]{article}

\input{ ../../../preambules/mes_packages_lua }
\input{ ../../../preambules/style_cours }
\fancyhead[L]{\textbf{C2.} \textcolor{gray!70}{Première spécialité}}
\fancyhead[R]{\themeMatiere}
%
% -------------------------------------------------------------------------------
%
\begin{document}
\titre{Composition d'un système chimique}
\section{Quantité de matière et grandeurs molaires}
\subsection{Quantité de matière}

Pour compter les entités chimiques, on les regroupe par "paquets" qu'on appelle des
\textbf{moles}.

\begin{minipage}{.7\linewidth} Une mole contient \num{6.02e23} entités chimiques.
  
  Ce nombre est appelé le \textcolor{red}{\textbf{nombre d'Avogadro}}, noté $ N_a $.
  \[
    N_a = \qty{6.02e23}{\per\mole}
  \]
\end{minipage}
%
\begin{minipage}{.2\linewidth}
  \includegraphics[scale=.3]{./img/avogadro}
\end{minipage}

\begin{aConnaitre}
  \begin{listeAC}
    \item La quantité de matière a pour symbole $\n$ et s'exprime en mole, notée
    \textbf{mol}.
    \item Elle \textbf{ne se mesure pas directement} et doit donc être calculée.
  \end{listeAC}
\end{aConnaitre}
%
\subsection{Masse molaire}
\begin{aConnaitre}
  \begin{listeAC}
    \item La \textbf{masse molaire atomique} $M$ d'un élément chimique est \textbf{la
      masse d'une mole de cet élément.}
    %
    \item La \textbf{masse molaire d'une espèce chimique} $ M $ est
    égale \textbf{à la somme des masses molaires atomiques} de tous les
    atomes qui composent la molécule.
    % 
    \item La \textbf{masse molaire ionique} peut être assimilée à
    la \textbf{masse molaire atomique} car la masse des électrons est
    négligeable devant celle de l'atome.
    % 
    \formule{
      $M=\dfrac{m}{n}$ \textcolor{black}{$\Leftrightarrow$} $n = \dfrac{m}{M}$ }
    { $M$ : $\unit{\g\per\mole}$ \\ $m$ : \unit{g} \\ $n$ : \unit{\mole
      }}
  \end{listeAC}
\end{aConnaitre}
%
\notebox{Les valeurs des masses molaires atomiques sont données dans le tableau
périodique.}
\begin{figure}[ht]
  \newcommand \hauteur {1.8}
  \newcommand \largeur{1.8}
  \centering
  \begin{tikzpicture}[pin distance=1cm, pin edge={<-}]
    \draw (0,0) rectangle (\largeur, \hauteur);
    \node[pin=right:symbole] (cl) at (\largeur/2, \hauteur/2) {\Huge \bfseries Cl};
    \node[pin=left:numéro atomique] (z) at (.3, \hauteur - .3) {17};
    \node[pin=right:masse molaire] (M) at (\largeur - .4, \hauteur - .3) {$35,5$};
    \node[pin=left:nom] (nom) at (\largeur/2 , .3) {Chlore};
  \end{tikzpicture}
\end{figure}
%
\begin{ex}{Masse molaire de l’eau}
  $\textcolor{blue}{M_{\ch{H2O}}} = 2\times \textcolor{orange}{M_{\ch{H}}}
  + \textcolor{orange}{M_{\ch{O}}}$
\end{ex}
%
\begin{ex}{Masse molaire du glucose}
  $\textcolor{blue}{M_{\ch{C6H{12}O6}}} = 6\times \textcolor{orange}{M_{\ch{C}}} + 12\times
  \textcolor{orange}{M_{\ch{H}}} + 6 \times \textcolor{orange}{M_{\ch{O}}} $
\end{ex}
%
\begin{ex}{Masse molaire de l’ion sulfate}
  $ \textcolor{orange}{M_{\ch{SO_4^{2-}}}} = \textcolor{orange}{M_{\ch{S}}}
  + 4 \times \textcolor{orange}{M_{\ch{O}}} $
\end{ex}
% %
\subsection{Détermination de la quantité de matière pour un corps pur liquide}

La masse volumique est \textbf{la masse d'un composé par unité de volume.}
\formule{$\rho_\text{corps} = \dfrac{m_\text{corps}}{V_\text{corps}}$}
{$m_\text{corps}$ : \unit{g} \\ $V_\text{corps}$ : \unit{L} \\ $\rho_\text{corps}$ : \unit{\g\per\L}}
%
\begin{multicols}{2}
  \begin{aConnaitre}
    On peut exprimer la quantité de matière d'un corps pur en fonction de \textbf{son
      volume}, de \textbf{sa masse volumique} et de \textbf{masse molaire} :
    \begin{center}
      \cadre{\color{red!75!black}$n_\text{corps}=\dfrac{\rho_\text{corps} \times
          V_\text{corps}}{M_\text{corps}}$}
    \end{center}
  \end{aConnaitre}
\columnbreak
\begin{demo}
  \[ 
    M = \dfrac{m}{n} \Leftrightarrow n = \dfrac{m}{M} \hspace{.5cm}\textcolor{red}{(*)}
  \]
  Or, la masse volumique $\rho$ s'écrit : 
  \[ 
    \rho = \dfrac{m}{V} \Leftrightarrow m = \rho \times V
  \]
  En remplaçant $m$ dans \textcolor{red}{(*)} , on obtient $ n $:
  \[
    n=\dfrac{\rho \times V}{M}
  \]
\end{demo}
\end{multicols}
%
\subsection{Volume molaire}
\begin{aConnaitre}
  \begin{listeAC}
    \item Le volume molaire est \textbf{le volume occupé par une mole de gaz}.
    \item Ce volume dépend de la température et de la pression, mais \textbf{pas de la
      nature du gaz}.
    %
    \formule{$V_m = \dfrac{V}{n} $}{$V_m$ : \unit{\L\per\mol}\\ $ V$ : \unit{L}  \\ $ n$ : \unit{\mol}  }
  \end{listeAC}	 
\end{aConnaitre}
%
\begin{ex}{}
  À pression de 1 bar (1 073 hPa), le volume occupé par un mole d'un gaz est :
\end{ex}
%
\begin{center}
  \begin{tabular}{ccc}
    \textcolor{blue}{\textbf{Température}}
    & \textbf{ $ \qty{0}{\degreeCelsius} $}
    & \textbf{ $ \qty{20}{\degreeCelsius} $} \\\toprule
    \textbf{$ V_m $}
    & $ \qty{22.4}{\L\per\mole} $
    & $ \qty{24.0}{\L\per\mole} $ \\
    \bottomrule
  \end{tabular}
\end{center}
%
% -------------------------------------------------------------------------------
%
\section{Concentration en masse et en quantité de matière}
\subsection{Rappel: concentration en masse}
La concentration en masse d'un soluté $\gamma_{\text{i}}$ est la masse de
  ce soluté $ mi_{\text{i}} $ dans \qty{1}{L} de solution:
	
\formule{$\gamma_{\text{i}}=\dfrac{m_{\text{i}}}{V_\text{sol}}$} {$\gamma_{\text{i}}$ :
\unit{\g\per\L} \\ $m_{\text{i}}$ : \unit{\g} \\ $V_{sol}$ : \unit{\L}}
%
\warningbox{
  la relation ressemble à la définition de la masse volumique, mais il s'agit ici de
  \textbf{la masse de soluté} et non \textbf{pas de la masse de liquide}.
}
%
\subsection{Concentration en quantité de matière}
\begin{aConnaitre}[sidebyside, lefthand ratio = .5]
  La \textbf{concentration en quantité de matière} est la quantité de matière d'un soluté
  par litre de solution.
  %
  \formule{$c_{\text{i}} = \dfrac{n_{\text{i}}}{V_\text{sol}}$} {$n_{\text{i}}$ : \unit{mol}
    \\$V_\text{sol}$ : \unit{L} \\
    $c_{\text{i}}$ : \unit{\mol\per\L} } \tcblower La \textbf{concentration en masse} et la
  concentration en quantité de matière sont liées par la relation:
  %
  \formule{$M= \dfrac{\gamma}{c}$}
  {$M$ : \unit{\g\per\mol} \\ $ \gamma $ : \unit{\g\per\L}  \\ $ c $ : \unit{\mole\per\L}  }	
\end{aConnaitre}
%
\subsection{La dilution}
\begin{figure}[ht]
  \centering
  \begin{minipage}{.8\linewidth}
    \centering
    \includegraphics[scale=.5]{./img/dilution}
    \caption{schéma d'une dilution}
  \end{minipage}
  \begin{minipage}{.15\linewidth}
    \centering
    \qrcode{https://youtu.be/PTGqcaHo5us?feature=shared} 
  \end{minipage}
\end{figure}
%
\begin{aConnaitre}[sidebyside, lefthand ratio = .5]
  Pour préparer une solution par dilution, on utilise la
  \textbf{\textcolor{red}{conservation de la quantité de matière de soluté}}.
  \begin{center}
    \cadre{$c_\text{mère} \times V_\text{mère} = c_\text{fille} \times V_\text{fille}$}
  \end{center}
  \tcblower On peut calculer \textbf{le facteur de dilution} et en déduire le volume de
  solution mère à prélever.
  \begin{center}
    \cadre{$f=\dfrac{c_\text{mère}}{c_\text{fille}} = \dfrac{V_\text{fille}}{V_\text{mère}}$}
  \end{center}
\end{aConnaitre}
% 
% -------------------------------------------------------------------------------
%
\section{Couleur et concentration}
\subsection{Absorbance}
\begin{aConnaitre}
  \begin{listeAC}
    \item À une longueur d'onde donnée, l'\textbf{absorbance $A$} quantifie la
    proportion de radiation absorbée.
    \item On appelle \textbf{spectre d'absorbance} d'une espèce chimique la
    courbe représentant $A$ en fonction de $\lambda$.
    \item En première approximation, on peut dire que \textbf{la couleur d'une
      solution correspond à la couleur complémentaire de la longueur d'onde d'absorption
      maximale $\lambda_{max}$} (exprimée en \unit{\nano\m}).
  \end{listeAC}
\end{aConnaitre}
%
\begin{multicols}{2}
  \begin{ex}{spectre du \ch{CoCl4^2-} à \qty{1e-3}{\mole\per\l}}
  \centering
  \includegraphics[width=.9\textwidth]{./img/spectre_COCl4}
  \end{ex}
  La solution de \ch{CoCl4^2-} est de couleur bleue
  \columnbreak
  \begin{ex}{cercle chromatique simplifié}
  \centering
  \includegraphics[width=.9\textwidth]{./img/echelleCouleurLambda}
  \end{ex}
\end{multicols}
%
\subsection{Loi de Beer-Lambert}
\textbf{L'absorbance d'une solution dépend de :}
\begin{itemize}[label = \ding{237}]
  \begin{multicols}{2}
    \item La nature de l'espèce chimique absorbante.
    \item La \textcolor{red}{longueur $l$} de la solution traversée.
    \columnbreak
    \item La \textcolor{red}{longueur d'onde $ \lambda $} du rayonnement.
    \item La \textcolor{red}{concentration $c$} de l'espèce absorbante.
  \end{multicols}
\end{itemize}
% \clearpage

\begin{ex}[]{}
  Spectre du \ch{CoCl4^2-} à différentes concentrations
  \tcblower
  \centering
  \includegraphics[scale=.6,valign=t]{./img/spectre_COCl4_c}
\end{ex}

\begin{aConnaitre}
  \begin{center}
    \textbf{LOI DE BEER-LAMBERT}
  \end{center}

  Pour une longueur d'onde fixée, \textbf{l'absorbance est proportionnelle à la
    concentration de l'espèce absorbante} :
  %
  \formule{$A = \varepsilon_{\lambda} \times l \times c$}
  { $A$ : sans unités \\ $l$ : \unit{\centi\m} \\ $c$ : \unit{\mol\per\l} \\
    $\varepsilon_{\lambda}$ : \unit{\l\per\mol\per\centi\m}}
\end{aConnaitre}
%
\begin{itemize}
  \item $\varepsilon_{\lambda}$ est le coefficient d'absorption molaire. Il quantifie
  l'absorbance d'une espèce chimique à une longueur d'onde donnée.
  \item Cette relation reste valable pour des concentrations relativement faibles
  (inférieures à $10^{-2}$ \unit{\mole\per\litre})
\end{itemize}
%
% -------------------------------------------------------------------------------
%
\section{Dosage par étalonnage}
Doser une espèce chimique en solution, c'est \textcolor{red}{\textbf{déterminer avec
    précision sa quantité de matière dans un volume de solution donné}}.
%
\begin{aConnaitre} \textsc{protocole d'un dosage par étalonnage}
  \begin{listeAC}
    \item Mesurer l'absorbance de solutions de concentrations connues et
    tracer la courbe d'étalonnage $A=f(c)$.
    \item Mesurer l'absorbance de la solution à doser.
    \item Par lecture graphique, ou en utilisant l'équation de la courbe de
    tendant, déterminer la concentration de cette solution.
  \end{listeAC}
\end{aConnaitre}
%
\begin{figure}[ht]
  \begin{ex}{}
    Tableau de valeurs et courbe d'étalonnage associée
  \end{ex}
  %
  \begin{minipage}{.35\linewidth}
    \begin{center}
      \rowcolors{2}{lightgray}{}
      \begin{tabular}{*{2}{c}}
        $ c $ / $ \times 10^{-5} $ \unit{\mol\per\L} & A  \\
        \toprule
        3,0 & 0,068 \\
        4,0 & 0,073 \\
        5,0 & 0,011 \\
        6,0 & 0,115 \\
        8,0 & 0,151 \\
        10,0 & 0,249 \\
        \bottomrule
      \end{tabular}
    \end{center}
  \end{minipage}
  \begin{minipage}{.6\linewidth}
    \centering
    \begin{tikzpicture}
      \begin{axis}[
        width  = .95 \linewidth,
        height = 7.5cm,
        ymin=0,%ymax=.2,
        xmin=0,%xmax=1.2e-4,
        xlabel=$c/\unit{\mol\per\l}$,
        ylabel=$A$,
        grid = both,
        minor tick num=4,
        % tickpos=left,
        % scaled ticks=true, % pour ajouter puisssance de 10 globale
        % tick scale binop=\times, % avec le signe "x"
        % domain=0:6,
        % samples=10,
        minor grid style = {color=lightgray!20},
        major grid style = {color = lightgray},
        % xtick distance = 1,
        % axis line shift = 5 pt, % pour un style différence
        legend pos = north west,
        legend entries={Points expérimentaux, $A=\num{2.1e3} \times c$}
        ]
        \addplot[only marks,red, mark=*] table[x=c, y=A,col sep=comma] {valeurs_dosage.csv};
        \addplot[blue, domain=0:1.2e-4]{2.1e3*\x};
        % \legend()
      \end{axis}
    \end{tikzpicture}
  \end{minipage}
\end{figure}
\end{document}
