import matplotlib.pyplot as plt
from scipy.stats import linregress

C = [1e-2, 2.5e-2, 5e-2, 8e-2, .1]
A = [.114, 0.28, .48, .91, 1.14 ]

p = linregress(C, A)

C_mod = [0] + C
A_mod = [p[0] * C_mod[i] + p[1] for i in range(len(C_mod))]

print(p)

plt.figure()
plt.plot(C, A, "o", label="Points expérimentaux")
plt.plot(C_mod, A_mod, "r-", label=f"Molélisation: A = {p[0]:.2f}C  {p[1]:.2f}")

plt.xlim(0)
plt.ylim(0)
plt.title("Courbe d’étalonnage")
plt.xlabel("C / $mol \cdot L^{-1}$")
plt.ylabel("A")

plt.grid()
plt.legend()
plt.show()
