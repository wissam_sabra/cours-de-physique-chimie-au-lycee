\documentclass[french]{exam}
\input{../../../preambules/mes_packages_lua}
\input{../../../preambules/style_pdt}
\header{\textbf{Chapitre 4.} \textcolor{gray!60}{Première spécialité}} 
{}
{\themeMatiere}
% \printanswers
% -------------------------------------------------------------------------------
\begin{document}
\titre{Plan de travail}
% -------------------------------------------------------------------------------
\section{\aRevoir}
\begin{revisions}
  \notionsRev{
    \begin{itemize}
      \item Tableau périodique
      \begin{center}
        \qrcode{https://www.youtube.com/watch?v=L6QmzMCl8JQ}
      \end{center}
      \item Configuration électronique
      \begin{center}
	    \qrcode{https://www.youtube.com/watch?v=iDNyt-rcGWY}
      \end{center}
      \item Électrons de valences
      \item Stabilité des gaz nobles
      \item Ions monoatomiques
      \item Schéma de Lewis
      \begin{center}
        \qrcode{https://www.youtube.com/watch?v=kZkDhbsMbzk}
        \hspace{1cm}
        \qrcode{https://www.youtube.com/watch?v=bmV-Tbv2Me8}
      \end{center}
    \end{itemize}
  }
  \exosRev{
    \begin{itemize}
      \item Le cortège électronique :
      
      \qrcode{https://www.lelivrescolaire.fr/page/6225358}
      \url{lelivrescolaire.fr/page/6225358}
      %
      \item Stabilité des entités chimiques
      
      \qrcode{https://www.lelivrescolaire.fr/page/6225400}
      \url{lelivrescolaire.fr/page/6225400}
    \end{itemize}
  }
\end{revisions}
% -------------------------------------------------------------------------------
\begin{activites}
  \centering
  \act{AE1}
  \begin{itemize}
    \item Représentation de Lewis
    \item Géométrie
  \end{itemize}
\end{activites}
% -------------------------------------------------------------------------------
\section{\jeMapproprie}
\begin{enumerate}
  \item Donner la configuration électronique de l'atome d'aluminium \ch{13Al}. Préciser la
  couche de valence.
  \begin{solutionordottedlines}[3cm]
    Le noyau d'aluminium contient 13 protons. L'atome possède donc 13 électrons :
    
    \ch{13Al} : $ 1s^2 ~ 2s^2 ~ 2p^6 ~ \bm{3s^2} ~ \bm{3p^1} $
  \end{solutionordottedlines}
  % 
  \item En déduire sa position dans la classification périodique. Préciser à quel bloc il
  appartient
  \begin{solutionordottedlines}[3cm]
    La couche de valence est la couche 3 : il se trouve donc dans la troisième ligne.  La
    sous couche p contient 1 électrons, il se trouve donc dans la première colonne du bloc
    p, soit la colonne 11.
  \end{solutionordottedlines}
  % 
  \item A partir de sa position dans le tableau périodique, donner la formule des ions
  monoatomiques formés par le magnésium Mg. Justifier la réponse
  \begin{solutionordottedlines}[3cm]
    Le magnésium est dans la deuxième colonne : il formera donc un ion \ch{Mg^2+} pour obtenir la 
    configuration électronique du Néon.
    \ch{Mg^2+} : $ 1s^2 ~ 2s^2 ~ 2p^6 $
  \end{solutionordottedlines}
  % 
  \item Donner le schéma de Lewis de : \ch{O2}, \ch{Cl-} et \ch{HCl}
  \begin{solutionorbox}[5cm]
    \hspace{1cm}
    \chemfig{\charge{135=\|, -135=\|}{O}=\charge{45=\|, -45=\|}{O}}
    \hspace{1cm}
    \chemfig{\charge{0=\|, 90=\|, 270=\|, 180=\|, 45[anchor=180+\chargeangle]=$ \ominus $}{Cl}}
    \hspace{1cm}
    \chemfig{H-\charge{0=\|, 90=\|, 270=\|}{Cl}}
  \end{solutionorbox}
  % 
  \item Comment varie l'électronégativité des éléments chimiques quand on descend dans une colonne 
  tu tableau périodique? Quel est l'élément le plus électronégatif?
  \begin{solutionordottedlines}[3cm]
    L'électronégativité diminue quand on descend dans la classification et l'élément le plus 
    électronégatif est le fluor.
    % 
  \end{solutionordottedlines}
  \item Montrer qu'une liaison \ch{C-H} n'est pas polarisée.
  % 
  \begin{solutionordottedlines}[2cm]
    $ \chi{C} - \chi(H) = 2,55 - 2,2 = 0,35 < 4  $
    
    Donc la liaison \ch{C-H} n'est pas polarisée.
  \end{solutionordottedlines}
\end{enumerate}
% ----------------------------------------------------------------------------------------------
% \clearpage
\section{\jesaisfaire}
\manuel{74-80} : Polarité et géométrie

\begin{jeSaisFaire}
  \capacite{Établir le schéma de Lewis de molécules et d'ions à partir du tableau périodique.}
  {12, 13, 14, 15, 43 }
  M
  \capacite{Interpréter la géométrie d'une molécule à partir de son schéma de Lewis.}
  {19 }
  %
  \capacite{Déterminer le caractère polarisé d'une liaison à partir de l'électronégativité
    des atomes.}  {24}
  %
  \capacite{Déterminer le caractère polaire d'une molécule }
  {34, 39}
  %
  \capacite{\textbf{Exp.} Utiliser un logiciel de représentation ou des modèles moléculaires pour 
    visualiser la géométrie d'une entité.}{AE, 39}
  %
  \midrule
  %-------------
\end{jeSaisFaire}
\end{document}