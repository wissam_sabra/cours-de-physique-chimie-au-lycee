\documentclass[french]{article}
\input{../../../preambules/mes_packages_lua}
\input{../../../preambules/style_cours}
\fancyhead[L]{\textbf{M2.} \textcolor{gray!60}{Première spécialité}}
\fancyhead[R]{\themeMvt}
%-------------------------------------------

\begin{document}
\titre{\textbf{Chapitre 8.} Description d’un fluide au repos}
%----------------------------------------------------
\section{Fluide au repos}
\subsection{Généralités}
\begin{aConnaitre}
  \begin{listeAC}
    \item Un fluide est un \textbf{corps susceptible de s’écouler}. Il 
    \textbf{n’a pas de forme propre} et prend celle du récipient qui le 
    contient.
    \item Dans le cadre de ce cours, un fluide correspond aux \textbf{états 
      liquides et gazeux}.
    \item À l’échelle macroscopique, un fluide au repos n’a pas de 
    mouvement d’ensemble. 
    \item En revanche, à l’\textbf{échelle microscopique}, les entités qui 
    le constituent sont \textbf{en mouvement incessant et désordonné}.
  \end{listeAC}
\end{aConnaitre}
% 
À l'échelle macroscopique, \textbf{un fluide} est décrit par \textbf{trois grandeurs
  physiques} :
\begin{multicols}{3}
  \begin{dinglist}{43}
    \item La température $ T $
    \item la masse volumique $ \rho $
    \item La pression $ p $
  \end{dinglist}
\end{multicols}
Ces grandeurs rendent compte du \textbf{comportement microscopique des 
  constituants} d'un fluide.
%
\begin{multicols}{2}
  \begin{ex}{Modèles microscopiques des fluides.}
    Gaz (air) et eau (liquide).
    \centering
    
    \includegraphics[scale=0.1]{./img/fluideModeleMoleculaire}
  \end{ex}
  \columnbreak
  \begin{ptMath}
    % \setlength{\baselineskip}{1.5\baselineskip}
    \begin{center}
      \textcolor{blue!55!green}{\textbf{Unités de pression}}
    \end{center}
    \begin{dinglist}{217}
      \item En plongée sous-marine : le bar.
      \[
        \qty{1}{bar} = \qty{1e5}{\Pa} 
      \]      
      \item En météorologie : Pression atmosphérique.
      \[
        1~\text{atm} = \qty{1.013}{bar} = \qty{1.013e5}{\Pa}
      \]
    \end{dinglist}
  \end{ptMath}
\end{multicols}
%
\subsection{Grandeurs de description d'un fluide}
\begin{center}
  \renewcommand{\arraystretch}{2}{
    \begin{tabular}{|m{8cm}|m{8cm}|}
      \hline
      \rowcolor{gray!40} \centering \textbf{Échelle macroscopique} 
      & \centering \textbf{Échelle microscopique} 
	\tabularnewline\hline
        %
	\textbf{Température $ T $} (en \unit{\degreeCelsius} ou kelvins noté 
	\unit{\K} ) mesurée avec un thermomètre. 
	\newline \textcolor{blue}{$ T  $ (en  \unit{\K} ) = $ \theta $ (en 
	\unit{\degreeCelsius} ) +  \num{273,15}  }
        %
      & \textbf{Agitation des entités} : 
	une agitation plus importante correspond à une température plus élevée. 
      \\\hline
      \textbf{Masse volumique $ \rho $} (en $ \unit{\kg\per\m\cubed} $)
      %
      & \textbf{Proximité des entités} : plus elles sont proches, plus la 
        masse volumique est grande. 
      \\\hline
      %
      \textbf{Pression $ P $} (en pascal \unit{Pa}) mesurée avec un manomètre 
      & \textbf{Chocs entre les entités:} plus il y a de chocs, et plus la 
        pression est importante.
      \\\hline
    \end{tabular}
  }
\end{center}
%--------------------------------------------------
\clearpage
\section{Pression et force pressante}
\begin{aConnaitre}
	L'action exercée par un fluide \textbf{sur une paroi} est modélisée par une 
	force nommée \textbf{une force pressante}.
\end{aConnaitre}
%suite aConnaitre
\begin{figure}[ht]
\begin{minipage}{0.5\linewidth}
	\begin{aConnaitre}
	Caractéristiques de la force pressante sur une surface S :
	\begin{itemize}
		\item \textbf{Direction }: orthogonale à la paroi
		\item \textbf{Sens} : du fluide vers la paroi
		\item \textbf{Norme} : valeur de la force déterminée par l'expression :
	\end{itemize}
	\formule{$  F = P \times S $ }
		{$F$ / \unit{\newton}\\	
		$P$ / \unit{\pascal} ou \unit{\N\per\m\squared}\\	
		$ S $ / \unit{\m\squared}
	}
	\textit{Expression vectorielle : $ \vv{F} = P \times S  \times 
	\textcolor{green!40!black}{\vec{u_n}}   $}
\end{aConnaitre}
\end{minipage}
%
\begin{minipage}{0.45\linewidth}
  \centering
  \begin{tikzpicture} [scale=.8]
    % \draw [help lines] (0,0) grid (6,5);
    % Paroi
    \draw [ultra thick] (3,0) -- (3,4) node [above] {Paroi} ;
    \draw (1,3) node [cyan!70!black] {Fluide} ;
    % Fluide
    \draw [cyan!70!black, dashed, ->,thick] (2,2.5) node {$ \bullet $} -- 
    (3,2) -- (1,0.5)   ;
    \draw [cyan!70!black, dashed, ->, thick] (1,2) node {$ \bullet $} -- 
    (3,1) -- (2,0)   ;
    \draw [cyan!70!black, dashed, ->, thick] (2.5,3) node {$ \bullet $} -- 
    (3,2.5) -- (2.5,2)   ;
    % Vecteur
    \draw [ultra thick, red, ->] (3,2) -- (5.5,2) node [above, near 
    end] {$ \vv{F} $};
    \draw [ultra thick, dashed, green!40!black, ->] (3,2) -- (4,2) 
    node [below,midway] {$ \vv{u_n} $};
  \end{tikzpicture}
  \caption{Représentation force pressante}
  \label{forcePressante} %renvoi à la fig dans un texte :  \textbf{voir FIG 
  % \ref{forceGrav}}		
\end{minipage}
\end{figure}

\vskip .8cm
%----------------------------
\section{Loi de Mariott : modèle de comportement d'un gaz}
\begin{aConnaitre}
  \textsc{loi de Mariott}
  
  À température et quantité de matière constantes, le \textbf{produit de la
    pression P} d’un gaz \textbf{par le volume} qu’il occupe V est 
  \textbf{constant}: 
  \begin{center}
    \cadre {\textcolor{red}{$ P \times V = \text{constante} $}}
  \end{center}
\end{aConnaitre}
%
\begin{ex}{}
\end{ex}
\begin{multicols}{3}
  \includegraphics[scale=0.07]{./img/seringueMariotte}
  \columnbreak
  
  \ding{217} Lorsqu'on diminue le volume de la seringue par déplacement du 
  piston, on ressent que la pression exercée sur le bouchon augmente.

  \ding{217} En plongée, la pression de l'eau augmente avec la profondeur et 
  le volume du ballon diminue.
  \columnbreak
  
  \includegraphics[scale=0.25]{./img/mariotte3}
\end{multicols}

\begin{figure}[ht]
  \begin{minipage}{0.5\linewidth}
    % \includegraphics[scale=0.7]{./img/graphiquePV}
    \begin{tikzpicture}
      \begin{axis}[
        width  = .95 \linewidth,
        height = 6cm,
        xmin = 0 ,% xmax = ,%
        ymin = 0, % ymax =,%
        xlabel = $\frac{1}{V}$ / \unit{\per\mL} ,%
        ylabel = $P$ / hPa ,%
        grid = both,
        minor tick num=4,
        tickpos=left,
        legend entries = {Valeurs expérimentales, modèle},
        scaled ticks=true, % pour ajouter puisssance de 10 global
        tick scale binop=\times, % avec le signe "x"
        minor grid style = {color=lightgray!20},
        major grid style = {color = lightgray},
        legend pos =  north west,
        ]
        \addplot[only marks] table[x=invV, y=P, col sep=comma]{mariott.csv};
        \addplot[no markers, domain=0:0.04, red]{3.3e4 * \x + 181};
      \end{axis}
  \end{tikzpicture}
    \caption{}
    \label{loiMariotte}
  \end{minipage}
  \begin{minipage}{0.4\linewidth}
    \textcolor{blue}{Autre formulation : À température et quantité de matière 
      constante, la pression du gaz est inversement proportionnelle au volume 
      occupé par ce  gaz.}
    \begin{center}
      \textcolor{red}{$ P = Cte \times \dfrac{1}{V} $}
    \end{center}

    On peut noter que les points expérimentaux sont bien alignés mais le modèle n’est pas
    tout à fait linéaire: cela peut s’expliquer par une erreur systématique lors des
    mesures (appareils mal calibré part exemple).
  \end{minipage}
\end{figure}
%----------------------
\section{Loi fondamentale de la statique des fluides}
\subsection{Fluide au repos}
À l'échelle microscopique, les entités d'un fluide sont en perpétuelle agitation.

Un \textbf{fluide au repos} est dépourvu de mouvement global (déplacement de l'ensemble du
fluide) ou de mouvement interne observable (déplacement de parties du fluide :
\textit{tourbillon, vagues}).
% 
\subsection{Fluide incompressible}
\begin{aConnaitre}
  Un \textbf{fluide incompressible} possède une masse volumique $ \rho $ indépendante de
  la pression $ p $, lorsque sa température est constante.
  \begin{listeAC}
    \item Les liquides sont quasiment incompressibles.
    \item Les gaz sont compressibles.
  \end{listeAC}
\end{aConnaitre}
% 
\subsection{Loi fondamentale de la statique des fluides incompressibles}
\begin{minipage}{.6\linewidth}
  \begin{aConnaitre}
    \begin{listeAC}
      \item La pression $ p $ en un point dépend de la profondeur et de la 
      masse volumique du fluide.
      \item La différence $ p_B - p_A $ des pressions entre deux points A et 
      B \textbf{est proportionnelle à la différence $ z_A-z_B $ de leurs 
        altitudes et à la masse volumique} du fluide (avec $ z_A > z_B $).
    \end{listeAC}

    \textsc{loi de la statique des fluides}
    \formule{$ p_B - p_A = \rho \times g \times (z_A - z_B) $}
    {$  p $ en \unit{Pa} \\
      $ \rho $ en \unit{\kg\per\m\cubed} \\
      $ g $ en \unit{\N\per\kg} \\
      $ z $ en \unit{\m} \\
    }
    \begin{center}
      \textit{g, intensité de pesanteur}
    \end{center}

    Cette relation peut également s’écrire :
    \formule{$\Delta p =  - \rho \times g \times \Delta z $ }{
      $\Delta p = p_2 - p_1$ \\
      $\Delta z = z_2 - z_1$}
  \end{aConnaitre}
\end{minipage}
% 
\begin{minipage}{0.4\linewidth}
  \centering
  \begin{tikzpicture} [scale=0.7]
    % \draw [help lines, gray!40] (0,0) grid (4,6);
    % axe
    \draw [thick, ->] (0,0) -- (0,6) node [above, left] {z} ;
    % rectangle
    \shade [top color=blue!10!cyan, bottom color = cyan!70!black, opacity=.5] (0.5,0)
    rectangle (3.5,5.5);
    % pointillé
    \draw [thick, gray!70, dashed] (0,4) -- (1.5,4) ;
    \draw [thick, gray!70, dashed] (0,1) -- (2.5,1);
    % point	
    \draw (0,1) node {\textbf{\_}} node [left]{$ z_B $} ;
    \draw (0,4) node {\textbf{\_}} node [left]{$ z_A $} ;
    \draw (1.5,4) node {\textbf{+}} node [right]{$ A$} ;
    \draw (2.5,1) node {\textbf{+}} node [right]{$ B $} ;
    
    % vecteur g et masse vol
    \draw [ultra thick, red,->] (4,3) --  (4,1) node [midway, right] {$ \vv{g}$} ; 
    \draw [ultra thick, cyan!70!black,<->] (-1,1) --  (-1,4) node [midway, left] {$ \Delta z $} ; 
    \draw (2,5) node {liquide ($ \rho $)} ;
  \end{tikzpicture}
  \captionof{figure}{}
  \label{loiStatique} 
\end{minipage}

%
\clearpage
\begin{ex}{}
\end{ex}

\begin{minipage}{0.55\linewidth}
  \begin{tikzpicture} [scale=.9]
    % \draw [help lines, gray!40] (0,0) grid (6,6);
    % axe
    \draw [thick, ->] (0,0) -- (0,6) node [above, left] {z} ;
    \draw [orange, thick, ->] (6.5,6) -- (6.5,0) 
    node [above, right] {Pression} ;
    % rectangle
    \shade [top color=blue!10!cyan, bottom color = cyan!70!black, 
    opacity=.5] (0.5,0) rectangle (6,3.5);
    % pointillé
    \draw [thick, gray!70, dashed] (0,3.5) -- (4,3.5) ;
    \draw [thick, gray!70, dashed] (0,1.5) -- (4,1.5);
    % point	
    \draw (0,1.5) node {\textbf{\_}} node [left]{$ z_1 $} ;
    \draw (0,3.5) node {\textbf{\_}} node [left]{$ z_0 $} ;
    \draw (4,3.5) node {\textbf{+}} node [right]{$ M_0 $} ;
    \draw (4,1.5) node {\textbf{+}} node [right]{$ M_1 $} ;
    \draw [orange] (6.5,1.5) node {\textbf{\_}} node [right]{$ P_1 $} ;
    \draw [orange] (6.5,3.5) node {\textbf{\_}} node [right]{$ P_0 $} ;
    % autre
    \draw [ultra thick, cyan!70!black,<->] (-1,1.5) --  (-1,3.5) 
    node [midway, left] {$ h $} ; 
    \draw (3.5,0.5) node {eau ($ \rho $)} ;
    % bateau
    \draw [shading angle=90](2,3.7) arc (-20:-160:0.5) ; 
    \draw [thick](1.5,3.7) -- (1.5,4.7) ;
    \fill (1.5,4) -- (2,4) -- (1.5,4.7) -- cycle;
    % personnage
    \fill[shift={(1.5,1.5)},rotate=-60]
    (0,0)ellipse(0.1 and 0.25) (0,0.35)circle(0.1);
    \draw [thick] (1.65,1.6) -- (1.7,1.3) ;
    \draw [thick] (1.3,1.35) -- (1.1,1.2) ;
    \draw [ultra thick,blue] (1.1,1.2) -- (1,1) ;
    \draw [ultra thick,red] (1.3,1.5) -- (1.6,1.7) ;
    \draw [green,thick] (1.7,1.8) -- (1.8,1.65)-- (1.9,1.6) ;
  \end{tikzpicture}
\end{minipage}
\begin{minipage}{0.4\linewidth}
  Entre un bateau situé au point $ M_0 $ à la surface de l'eau, à pression 
  atmosphérique $ P_0 $ et un plongeur situé au point $ M_1 $ à la profondeur 
  $ h = \Delta z = z_0 - z_1 $, la loi de la statique des fluides donne :
  \begin{center}
    $ P_1 = P_0 + \rho  g  h $
  \end{center}	
\end{minipage}
%\end{figure}

\textbf{Conséquence }:

La pression de l'eau augmente avec la profondeur. Dans l'eau, $\rho =
\qty{1e3}{\kg\per\m\cubed} $ et en prenant \mbox{$ g = \qty{10}{\N\per\kg} $}, on en déduit que
la pression $ P_1 $ du plongeur augmente d'environ 1 bar ($ = \qty{1e5}{\pascal} $) à
chaque fois que la profondeur augmente de \textcolor{orange}{10 mètres}.

$\WithArrows[tikz=blue]
P_1 &= P_0 + \textcolor{red}{\rho}  \textcolor{green!70!black}{g}  
\textcolor{orange}{h} \\
P_1 & = P_0 + \textcolor{red}{\num{1e3}} \times 
\textcolor{green!70!black}{10} \times \textcolor{orange}{10}\\
P_1 & = P_0 + \num{1e5} \Arrow{ $ \qty{1e5}{\pascal} = \qty{1}{\bar} $}\\
P_1 & = P_0 + \qty{1}{bar} 
\endWithArrows$
% 
\begin{figure}[ht]
  \centering
  % \includegraphics[scale = .9]{./img/graphiquePressionhauteur}
  \begin{tikzpicture}
    \begin{axis}[
      % scale=1.2,
      width  = .8 \linewidth,
      height = 7.5cm,
      xmin = 0 ,% xmax = ,%
      % ymin = 0, % ymax =,%
      xlabel = $h$ / m ,%
      ylabel = $P$ / Pa ,%
      grid = both,
      minor tick num=4,
      tickpos=left,
      scaled ticks=true, % pour ajouter puisssance de 10 globale
      scaled x ticks=base 10:{2},
      tick scale binop=\times, % avec le signe "x"
      % domain=0:6,
      % samples=10,
      minor grid style = {color=gray!30},
      major grid style = {color = gray!90},
      legend entries = {Points expérimentaux, Modèle : $P = \num{9.5e3} \times
        h + \num{9.95e5}$},
      legend pos =  north west,
      ]
      \addplot[only marks] coordinates{
        (0,         99500)
        (2e-2,         99700)
        (3e-2,         99800)
        (4e-2,         99900)
        (5e-2,         100000)
        (8e-2,         100300)
        (10.5e-2,      100500)
        (13e-2,        100700)
        (16e-2,        101000)
        (20e-2,        101400)
        (23e-2,        101700)
        (26e-2,        102000)
        (32e-2,        102500)
      };
      \addplot[no markers, domain=0:.35, red] {9.5e3 * \x + 99500};
      % \legend()
    \end{axis}(
  \end{tikzpicture}
  \caption{Pression en fonction de la 
    profondeur}
  \label{graphiquePeth} 
\end{figure}
\begin{ex}{Le tonneau de Pascal}
  \begin{minipage}{.85\linewidth}
    Blaise Pascal était un scientifique et phisolophe du XVII\ieme{} sciècle. Il prétent
    qu’un tonneau peut être détruit sous l’effet de la pression si on le rempli à l’aide
    d’un tube d’une hauteur suffisante.

    Pour savoir si c’est vrai, regarder la vidéo dont le lien est donné ci-contre.
  \end{minipage}
%
  \begin{minipage}{.1\linewidth}
    \centering
    \qrcode{https://www.youtube.com/watch?v=ET3irdmHsmc} 
  \end{minipage}
  \begin{center}
      \includegraphics[scale=.5]{ ./img/tonneau_pascal.png }
  \end{center}
\end{ex}
\end{document}
