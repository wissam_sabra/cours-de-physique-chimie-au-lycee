import csv

Volume = [40.0, 38.0, 36.0, 34.0, 32.0, 30.0, 42.0, 44.0, 46.0, 48.0, 50.0]
Pression = [1003.48, 1048.7, 1093.93, 1146.69, 1206.99, 1267.29, 965.789, 935.638,
            890.413, 860.262, 830.112]
inv_V = []
for V in Volume :
    inv_V.append(1/V)
print(inv_V)

with open('mariott.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerow(['V', 'invV', 'P'])
    for i in range(len(Volume)):
        ligne = [float(Volume[i]), float(inv_V[i]), float(Pression[i])]
        writer.writerow([Volume[i], round(inv_V[i],5), Pression[i]])
