\documentclass[french]{article}
\input{ ../../../preambules/mes_packages_lua }
\input{ ../../../preambules/style_cours }
\fancyhead[L]{\textbf{E2} \textcolor{gray!70}{Première spécialité}}
\fancyhead[R]{\themeEnergie}
% --------------------------------------------------------------------
\begin{document}
\titre{\textbf{Chapitre 12.1.} Travail et énergie cinétique.}
% ----------------------------------------------------
\section{Énergie cinétique}
\begin{aConnaitre}
  L'\textbf{énergie cinétique} $ E_c $ d'un système modélisé par un point
  matériel de masse $ m $ et de vitesse de valeur $ v $ dans un référentiel
  d'étude est définie par la relation :
  \formule{$ E_c = \dfrac{1}{2}mv^2 $}{
    $ m $ en kg \\
    $ v $ en \si{\m\per\s} \\
    $ E_c $ en joule J
  }
  \textit{Remarque : L'énergie cinétique est une grandeur
    positive ou nulle, qui dépend du référentiel considéré.}
\end{aConnaitre}
\begin{minipage}{.75\linewidth}
  \begin{ex}{le référentiel}
    Dans le référentiel du sol, la vache a une énergie cinétique nulle mais pas
    dans celui du train s'il roule.
  \end{ex}
\end{minipage}
\begin{minipage}{.2\linewidth}
  \centering \includegraphics[scale=.25]{./img/trainReferentiel}
\end{minipage}
%
\section{Travail d'une force constante}
Le travail d'une force exercée sur un système correspond à un \textbf{transfert
  d'énergie} entre un système et le milieu extérieur.

\textit{\textcolor{bleuAC}{La travail est souvent noté $ W $, initiale du mot
    anglais "work" qui signifie travail.}}
%
\subsection{Expression du travail d'une force constante}
\begin{aConnaitre}
  \begin{minipage}{.55\linewidth}
    \begin{listeAC}
      \item La force $ \vv{F} $ est \textbf{constante} si sa direction et son
      sens et sa norme sont inchangés au cours du déplacement.
      \item Le travail de A vers B de la force $ \vv{F} $ \textbf{constante} est
      le produit scalaire de cette force et du vecteur déplacement $ \vv{AB} $ :
    \end{listeAC}
  \end{minipage}
  %
  \begin{minipage}{.4\linewidth}
    \begin{center}
      \begin{tikzpicture} [scale=.7]
        \newcommand{\xmin}{0} \newcommand{\ymin}{0} \newcommand{\xmax}{6}
        \newcommand{\ymax}{4}
        %	\draw[help lines, dashed, color=black!15!] (\xmin,\ymin) grid
        % (\xmax,\ymax);
        % coordo A et B
        \coordinate (A) at (0,0) ; \coordinate (B) at (5,2) ; \coordinate (C) at
        (2,.8);
        % vecteur AB
        \draw [thick] (A) node [below] {A} -- (B) node [above] {B};
        % force F
        \draw [thick, ->, red] (A) node [black]{$ \bullet $} --++ (-10:2) node
        [above, near end] {$ \vv{F} $} ; \draw [thick, ->, red] (B) node
        [black]{$ \bullet $} --++ (-10:2) node [above, midway] {$ \vv{F} $} ;
        \draw [thick, ->, red] (C) --++ (-10:2) node [below, near end]
        {$ \vv{F} $} ;
        % arc
        \draw [thick, blue] (3,0.65) arc (0:30:1) node[midway, right]
        {$\theta$};
      \end{tikzpicture}
    \end{center}
  \end{minipage}
%		
  \formule{$ W_{\text{AB}}(\vv{F}) = \vv{F} \cdot \vv{AB} = F \times AB \times
    \cos(\theta)$}
  {$ W_{\text{AB}}(\vv{F}) $ en joule J \\
    $ F $ norme de la force en N \\
    $ AB $ distance parcourue en m\\
    $ \theta $ angle entre $ \vv{F} $ et $ \vv{AB} $}
\end{aConnaitre}
\begin{ex}{trois trajets}
  Sur les trois trajets, $ W_{\text{AB}}(\vv{F})$ est identique
  $ W_{\text{AB}}(\vv{F}) = \vv{F} \cdot \vv{AB} $.
  \begin{center}
    \begin{tikzpicture} [scale=1.1]
      \newcommand{\xmin}{0} \newcommand{\ymin}{0} \newcommand{\xmax}{6}
      \newcommand{\ymax}{4}
      %	\draw[help lines, dashed, color=black!15!] (\xmin,\ymin) grid
      % (\xmax,\ymax);
      % coordo A et B
      \coordinate (A) at (0,0); \coordinate (B) at (5,2); \coordinate (C) at
      (3,1.2);
      % arc
      \draw [thick, blue] (3.5,1.4) arc (0:35:.6) node[right] {$\theta$}; \draw
      [purple, thick, dashed](A) to[in=-110,out=90] (B); \draw[orange, thick,
      dashed] (A) to[bend right=45] (B);
      % vecteur AB
      \draw [thick] (A) node [black]{$ \bullet $} node [below] {A} -- (B) node
      [black]{$ \bullet $} node [above] {B};
      % force F
      \draw [thick, ->, green!55!blue] (C) --++ (55:1.2) node [above, midway,
      xshift = -3 pt] {$ \vv{F} $} ; \draw [thick, ->, green!55!blue] (1,1) --++
      (55:1.2) node [above, midway, xshift = -3 pt] {$ \vv{F} $} ; \draw [thick,
      ->, green!55!blue] (1.5,-.3) --++ (55:1.2) node [below, midway]
      {$ \vv{F} $} ;
    \end{tikzpicture}
  \end{center}
  Le travail d'une force constante ne dépend pas du chemin suivi entre les
  points A et B.
\end{ex}
%
\subsection{Travail moteur, nul ou résistant}
\begin{center}
  \renewcommand{\arraystretch}{1.5}
  \begin{tabular}
    {|>{\centering\arraybackslash}m{4.5cm}|>{\centering\arraybackslash}m{4.5cm}|>{\centering\arraybackslash}m{4.5cm}|}
    \hline
    \rowcolor{orange!10} \textbf{Travail moteur}
    & \textbf{Travail nul}
    & \textbf{Travail résistant}
    \\\hline
    \begin{tikzpicture}[scale=0.6]
      % coordo A et B
      \coordinate (A) at (0,0); \coordinate (B) at (4,0); \coordinate (C) at
      (2,0);
      % AB
      \draw [thick] (A) node [black]{$ \bullet $} node [left] {A} -- (B) node
      [black]{$ \bullet $} node [right] {B};
      % F
      \draw [thick, ->, green!55!blue] (C) --++ (40:2) node [above, midway,
      xshift = -3 pt] {$ \vv{F} $} ;
      % arc
      \draw [thick, blue] (3,0) arc (0:37:1) node[right] {$\theta$};
    \end{tikzpicture}
    &
      \begin{tikzpicture}[scale=0.6]
        % coordo A et B
        \coordinate (A) at (0,0); \coordinate (B) at (4,0);
        \coordinate (C) at (2,0);
        % AB
        \draw [thick] (A) node [black]{$ \bullet $} node
        [left] {A} -- (B) node [black]{$ \bullet $} node
        [right] {B};
        % F
        \draw [thick, ->, green!55!blue] (C) --++ (90:1.7)
        node [left, midway] {$ \vv{F} $} ;
        % arc
        \draw [thick, blue] (2.5,0) arc (0:90:0.5) node[right,
        xshift = 3pt, yshift = 3pt] {$\theta$};
      \end{tikzpicture}
    &
      \begin{tikzpicture}[scale=0.6]
        % coordo A et B
        \coordinate (A) at (0,0);
        \coordinate (B) at (4,0);
        \coordinate (C) at (2,0);
        % AB
        \draw [thick] (A) node
        [black]{$ \bullet $} node [left]
        {A} -- (B) node
        [black]{$ \bullet $} node [right]
        {B};
        % F
        \draw [thick, ->, green!55!blue]
        (C) --++ (140:2) node [above,
        midway, xshift = 3 pt]
        {$ \vv{F} $} ;
        % arc
        \draw [thick, blue] (2.5,0) arc
        (0:132:.5) node[right, xshift =
        5pt, yshift = 5pt] {$\theta$};
      \end{tikzpicture}
    \\ \hline
    $ 0 \leqslant \theta < \SI{90}{\degres} $
    & $ \theta = \SI{90}{\degres} $
    &$\SI{90}{\degres}<\theta\leqslant\SI{180}{\degres}$
    \\ \hline
    $ 0 < \cos(\theta) \leqslant 1 $
    & $ \cos(\theta) = 0 $
    &  $ -1 \leqslant \cos(\theta)< 0 $
    \\ \hline
    $ W_{\text{AB}}(\vv{F}) > 0 $
    & $ W_{\text{AB}}(\vv{F}) = 0 $
    & $ W_{\text{AB}}(\vv{F}) < 0 $
    \\ \hline
    La force favorise le déplacement
    & La force n'a pas d'effet
    & La  force ne favorise pas le déplacement
    \\ \hline
  \end{tabular}
\end{center}
\begin{minipage}{.75\linewidth}
  \begin{ex}{le téléski}
    La force exercée par le téléski sur le skieur effectue un travail moteur
    lors du déplacement du skieur.
  \end{ex}
\end{minipage}
\begin{minipage}{.2\linewidth}
  \centering \includegraphics[scale=.18]{./img/skieur}
\end{minipage}
%
\subsection{Exemple du travail du poids}
Soit un corps de masse $ m $ placé dans un champ de pesanteur uniforme
$ \vv{g} $. Ce corps est soumis à son poids $ \vv{P} $, force constante au
voisinage de la surface de la Terre.  \vskip .3cm Le travail du poids $ \vv{P} $
noté $ W_{\text{AB}}(\vv{P}) $ s'exprime :
\begin{center}
  \cadre{$ W_{\text{AB}}(\vv{P}) = \vv{P} \cdot \vv{AB} $}
\end{center}
%
\begin{minipage}{.6\linewidth}
  \textbf{Lors d'une descente de A vers B :}
%	

  {\textbf{Méthode 1} : avec le cosinus}
  % \vskip .3cm
  
  $\begin{WithArrows}[tikz = blue]
    W_{\text{AB}}(\vv{P}) &= \vv{P} \cdot \vv{AB} \\
    W_{\text{AB}}(\vv{P}) & = P \times AB \times \cos(\theta)
    \Arrow{avec $ P = m \times g $}\\
    & = m \times g \times AB \times \cos(\theta)
    \Arrow{$ \cos(\theta) = \dfrac{AC}{AB} $}\\
    & = m \times g \times AC \Arrow{avec $ AC = h  $}  \\
    W_{\text{AB}}(\vv{P}) & = m \times g \times h > 0
  \end{WithArrows}$
\end{minipage}
\begin{minipage}{.35\linewidth}
  \begin{center}
    \begin{tikzpicture}[thick,scale = .7]
      % repère
      \newcommand{\xmin}{0} \newcommand{\ymin}{0} \newcommand{\xmax}{6}
      \newcommand{\ymax}{5}
      %	\draw[help lines, dashed, color=black!15!] (\xmin,\ymin) grid
      % (\xmax,\ymax);
      \coordinate (O) at (0,0); \draw[thick, ->] (O) --++ (90:\ymax) node
      [right, at end] {$ z $}; \node at (0,1) [left] {$ z_B $} ; \node at (0,4)
      [left] {$ z_A $} ;
      % Points
      \coordinate (A) at (2,4); \coordinate (B) at (6,1); \coordinate (C) at
      (2,1); \coordinate (C') at (2,2);
		%
      \draw [gray, thick, dashed] (0,1) -- (B) ; \draw [gray, thick, dashed]
      (0,4) -- (A) ; \draw [gray, thick, dashed] (C) -- (A) ;
		%
      \draw [thick, ->, blue!85] (A) node [above] {A} -- (B) node [right] {B};
      \draw [thick, ->, purple] (A) -- (C') node [left, midway] {$ \vv{P} $};
      \draw [thick] (2.35,3.7) arc (-30:-80:.5) node[below, midway] {$\theta$};
      \node at (C) [below] {$ C $} ; \draw [thick] (2,1.4) -- (2.5,1.4) --
      (2.5,1) ; \draw [thick, <->] (0.9,1) -- (0.9,4) node [midway, left]
      {$ h $};
    \end{tikzpicture}
  \end{center}
\end{minipage}

\bigskip
\begin{center}
  \cadre{$ W_{\text{AB}}(\vv{P}) = m \times g \times (z_A - z_B) $ ~~~ lors
    d'une descente}
\end{center}
\vskip .5cm Le travail est \textbf{moteur lors de la descente} et
\textbf{résistant lors de la montée}. Il ne dépend que de la \textbf{différence
  d'altitude entre A et B}.

\begin{ptMath}
  \begin{multicols}{3}
    \centering
    \begin{tikzpicture}[scale=0.6]
      % triangle
      \fill [orange!10] (0,0) -- (4,0) -- (4,3) -- (0,0) ; \draw [thick] (0,0)
      -- (4,0) node [below,midway] {adjacent} ; \draw [thick] (0,0) -- (4,3)
      node [above, sloped,midway] {hypoténuse} ; \draw [thick] (4,3) -- (4,0)
      node [above,midway, sloped] {opposé} ; \draw (1.3,0) arc (0:45:1)
      node[right, midway] {$\alpha$};
    \end{tikzpicture}
	
    \columnbreak
    $ \cos(\theta) = \dfrac{\text{côté adjacent}}{\text{hypoténuse}} $ \vskip
    .5cm $ \sin(\theta) = \dfrac{\text{côté opposé}}{\text{hypoténuse}} $
		
    \columnbreak $ \cos(\dfrac{\pi}{2} + \theta) = - \sin(\theta) $
  \end{multicols}
\end{ptMath}
%
\begin{minipage}{.45\linewidth}
  \begin{center}
    {\bfseries Méthode 2 :} avec les coordonnées de vecteur
  \end{center}
  
  \[
    W_{\text{AB}}(\vv{P}) = \textcolor{purple}{\vv{P}} \cdot
    \textcolor{blue}{\vv{AB}}
  \]
	
  Coordonnées des vecteurs :
  \[
    \textcolor{purple}{\vv{P} \begin{pmatrix} 0 \\ - mg \end{pmatrix}} \text{ et
    } \textcolor{blue}{\vv{AB} \begin{pmatrix} x_B - x_A \\ z_B -
        z_A \end{pmatrix} }
  \]
  Donc :
  \begin{align*}
    W_{\text{AB}}(\vv{P}) & = \textcolor{purple}{\vv{P}} \cdot \textcolor{blue}{\vv{AB}} \\
    W_{\text{AB}}(\vv{P}) & = \textcolor{purple}{\begin{pmatrix}	0 \\ - mg	\end{pmatrix}} \times  \textcolor{blue}{\begin{pmatrix}
        x_B - x_A \\ z_B - z_A	\end{pmatrix}}  \\
    W_{\text{AB}}(\vv{P}) & = \textcolor{purple}{- mg} \times \textcolor{blue}{(z_B - z_A)} \\
    W_{\text{AB}}(\vv{P}) & = \textcolor{purple}{ mg} \times \textcolor{blue}{(z_A - z_B)} > 0 \\
  \end{align*}
\end{minipage}
\begin{minipage}{.45\linewidth}
  \begin{center}
    \begin{tikzpicture}[thick,scale = .6]
      % repère
      \newcommand{\xmin}{0} \newcommand{\ymin}{0} \newcommand{\xmax}{6}
      \newcommand{\ymax}{5}
      %	\draw[help lines, dashed, color=black!15!] (\xmin,\ymin) grid
      % (\xmax,\ymax);
      \coordinate (O) at (0,0); \draw[thick, ->] (O) --++ (90:\ymax) node
      [right, at end] {$ z $}; \draw[thick, ->] (O) --++ (0:\xmax) node [right,
      at end] {$ x $}; \node at (0,0) [left] {$ 0 $} ; \node at (0,1) [left]
      {$ z_B $} ; \node at (0,4) [left] {$ z_A $} ; \node at (5,0) [below]
      {$ x_B $} ; \node at (1,0) [below] {$ x_A $} ;
      % Points
      \coordinate (A) at (1,4); \coordinate (B) at (5,1); \coordinate (C) at
      (1,1); \coordinate (C') at (1,2);
			%
      \draw [gray, thick, dashed] (0,1) -- (B) ; \draw [gray, thick, dashed]
      (0,4) -- (A) ; \draw [gray, thick, dashed] (1,0) -- (A) ; \draw [gray,
      thick, dashed] (5,0) -- (B) ;
			%
      \draw [thick, ->, blue!85] (A) node [above] {A} -- (B) node [right] {B};
      \draw [thick, ->, purple] (A) -- (C') node [left, midway] {$ \vv{P} $};
      \draw [thick] (1.35,3.7) arc (-30:-80:.5) node[below, midway] {$\theta$};
    \end{tikzpicture}
  \end{center}
\end{minipage}
%
\subsection{Exemple d’une force de frottement constante sur une trajectoire
  rectiligne}
Les forces de frottements (solides ou fluides) sont toujours dans la même
direction que le mouvement et de sens opposé.

Le système se déplace d'une position A à une position B.
  
  \begin{minipage}{.5\linewidth}
    \centering
    \begin{tikzpicture}[scale=0.6]
      % coordo A et B
      \coordinate (A) at (0,0); \coordinate (B) at (5,0); \coordinate (C) at
      (3,0);
      % sens mvt
      \draw[gray, ->, thick] (1.5,2) -- (3.5,2) node [midway, above] {sens du
        mouvement} ;
      % AB
      \draw [thick] (-2,0) -- (7,0); \draw [ultra thick, blue, -> ] (A) node
      [above] {A} node [black]{$ \bullet $} -- (B) node [black]{$ \bullet $}
      node [above] {B}; \draw [thick] (C) node [below] {M} node {$ \bullet $} ;
      % F
      \draw [ultra thick, dotted, ->, red] (C) --++ (180:2) node [above, midway]
      {$ \vv{F} $} ;
      % arc
      \draw [thick] (3.5,0) arc (0:180:0.5) node[above, midway] {$\theta$};
    \end{tikzpicture}
  \end{minipage}
  %
  \begin{minipage}{.4\linewidth}
    \[
      W_{\text{AB}}(\vv{F}) = \vv{F} \cdot \vv{AB} = F \times AB \times
      \cos(\theta)
    \]
    Or, $ \theta = \SI{180}{\degres} $

    donc $ \cos(\theta) = - 1 $.
	
    Finalement,
    \[
      W_{\text{AB}}(\vv{F}) = - F \cdot AB
    \]
  \end{minipage}

  % \bigskip
  %
  \section{Théorème de l'énergie cinétique}
  \subsection{Énoncé du théorème de l'énergie cinétique}
  \begin{aConnaitre}
    La \textbf{variation de l'énergie cinétique $ \Delta E_c $} d'un système en
    mouvement d'un point A à un point B est égale à la somme des travaux des
    forces qu'il subit.
    \begin{center}
      \cadre{$ \Delta E_c = E_c(B) - E_c(A) = \sum W_{\text{AB}}(\vv{F}) $}
    \end{center}
  \end{aConnaitre}

  \textit{\textcolor{bleuAC}{\textbf{Remarques} : Ce théorème n'est valable que
      dans certains référentiels nommés référentiel galiléens : référentiel dans
      lequel le principe d'inertie s'applique. Pour des mouvements de courte
      durée, le référentiel terrestre est considéré comme un référentiel
      galiléen.}}

  \vskip .5cm
  \begin{grandCadre}
    \textbf{Méthode d'utilisation du théorème}
    \begin{dingautolist}{202}
      \item Définir le \textbf{système}.
      \item Préciser le \textbf{référentiel} dans lequel on étudie le mouvement.
      \item Faire le \textbf{bilan des forces} subies par le système en
      précisant (réaliser un schéma est conseillé) :
      \begin{itemize}[label = \ding{43}]
        \item Leur notation ($ P $, $ F_{\text{AB}} $, ...);
        \item Leur direction;
        \item Leur sens;
        \item L'expression de leur norme lorsqu'on la connaît.
      \end{itemize}
      \item Écrire l'expression littérale du \textbf{théorème de l'énergie
        cinétique} en utilisant les notations de l'énoncé.
    \end{dingautolist}
  \end{grandCadre}
%
  \subsection{Exemple de la chute libre}
  On modélise le mouvement d'une balle en chute libre lâchée sans vitesse
  initiale par le schéma suivant :
  \begin{center}
    \begin{tikzpicture}[thick,scale = .7]
      % repère
      \newcommand{\xmin}{0} \newcommand{\ymin}{0} \newcommand{\xmax}{6}
      \newcommand{\ymax}{5.5}
      %	\draw[help lines, dashed, color=black!15!] (\xmin,\ymin) grid
      % (\xmax,\ymax);
      \coordinate (O) at (0,0); \draw[thick, ->] (O) --++ (90:\ymax) node
      [right, at end] {$ z $}; \node at (0,1) [left] {$ z_B $} ; \node at (0,5)
      [left] {$ z_A $} ; \draw (2,4) node {$ \bullet $} node [left]{$ G $} ;
      % Points
      \coordinate (A) at (2,5); \coordinate (B) at (2,1);
		%
      \draw [gray, thick, dashed] (0,1) -- (B) node [black]{$ + $} node
      [right]{$ B $, $ v_B > v_A $}; \draw [gray, thick, dashed] (0,5) -- (A)
      node [black]{$ + $} node [right]{$ A $, $ v_A $};
		%
      \draw [thick, ->, purple] (2,4) -- (2,2) node [right, midway]
      {$ \vv{P} $};
    \end{tikzpicture}
  \end{center}
%
  \begin{dingautolist}{202}
    \item Dans le référentiel terrestre, supposé galiléen.
    \item On étudie une balle modélisé par son centre G.
    \item Le système en chute libre entre les points A et B n'est soumis qu'à
    son poids $ \vv{P} $ :
    \begin{itemize}[label = \ding{43}]
      \item $ \vv{P} $, vertical, vers le bas ;
      \item $ P = m \times g $.
    \end{itemize}
    \item Le \textbf{théorème de l'énergie cinétique} entre le point A et le
    point B s'écrit :
  \end{dingautolist}
%
  \begin{align*}
    \Delta E_c = E_c(B) - E_c(A) &= W_{\text{AB}}(\vv{P})  \\
    \dfrac{1}{2}mv_{B}^2 - \dfrac{1}{2}mv_{A}^2 & = m \times g \times (z_A - z_B) \\		
  \end{align*}
  La balle est lâchée sans vitesse initiale en A donc
  $ v_A = \SI{0}{\m\per\s} $, on obtient alors :
  \renewcommand{\CancelColor}{\color{red}}
  \begin{align*}
    \dfrac{1}{2}mv_{B}^2  & = m \times g \times (z_A - z_B) \\
    v_{B}^2 & = \dfrac{2 \times \cancel{m} \times g \times (z_A - z_B)}{\cancel{m}} \\
    v_{B} & = \sqrt{2g(z_A - z_B)}
  \end{align*}
\end{document}
