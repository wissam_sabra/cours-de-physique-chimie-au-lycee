\documentclass[french]{article}

\input{../../../preambules/mes_packages_lua}
\input{../../../preambules/style_cours}
\fancyhead[L]{\textbf{M1.} \textcolor{gray!70}{Première spécialité}}
\fancyhead[R]{\themeMvt}
% -------------------------------------------------------------------------------
\begin{document}
\titre{\textbf{Chapitre 7.} Interactions fondamentales et champs}
% 
\section{Interaction gravitationnelle}
\begin{minipage}{0.7\linewidth}
  Isaac Newton a énoncé la \emph{loi universelle de la gravitation} 
  au XVIII\ieme{} siècle. Cette loi décrit la gravitation comme une force
  responsable de la chute des corps et du mouvement des corps célestes,
  autrement dit, de l'\emph{attraction entre des corps ayant une masse}.
\end{minipage}
\begin{minipage}{.2\linewidth}
  \centering	
  \includegraphics [scale=0.2]{./img/newton.png}
\end{minipage}
%
\begin{aConnaitre}	
  L’interaction  gravitationnelle  entre  deux  points matériels 
  \textbf{\textcolor{blue}{A}} et \textbf{\textcolor{blue}{B}}, de masses 
  respectives $m_A$ et $m_B$, séparés par une distance d est modélisée par 
  des \textbf{forces de gravitation} \textcolor{red}{$ \vv{F_{A/B}}  $} et 
  \textcolor{red}{$ \vv{F_{B/A}}  $} dont \textbf{les caractéristiques} sont :
  \begin{itemize}	
    \item \textbf{Direction }: la direction de la droite (AB)
    \item \textbf{Sens} : de B vers A pour $ \vv{F_{A/B}}  $ et 
    réciproquement pour $ \vv{F_{B/A}}  $
    \item \textbf{Norme} : valeur de la force déterminée par l'expression : 
    \begin{center}
      \formule{
        $ F_{A/B} = F_{B/A} = G \times \dfrac{m_A \times m_B}{d^{2}} $}
      {
        $ F $ / \si{\newton}\\
        $ m $ / \si{\kg}\\
        $ d $ / \si{\metre}
      }
      G : Constante de gravitation universelle
      $ G = \qty{6,67 E-11}{\N\square\m\per\square\kg} $
    \end{center}
  \end{itemize}
\end{aConnaitre}
%schema
\begin{figure}[ht]
  \centering
  \input{ ./figures/force_gravit.tex }
  \caption{Représentation des forces de gravitation}
  \label{forceGrav}
\end{figure}
%
\begin{aConnaitre}
  \begin{minipage}{.45\linewidth}
    \centering
    \cadre {$ \vv{F_{B/A}} = G \times \dfrac{{m_A} \times {m_B}}{d^{2}} 
      \textcolor{green!40!black}{\vec{u_r}} $}
  \end{minipage}
  \begin{minipage}{.45\linewidth}
    \centering
    \cadre {
      $ \vv{F_{A/B}} = \textcolor{red}{-} G \times \dfrac{{m_A} 
        \times {m_B}}{d^{2}} \textcolor{green!40!black}{\vec{u_r}}$
    }
  \end{minipage}
\end{aConnaitre}
Remarque : $ \vv{F_{B/A}} $ = - $ \vv{F_{A/B}} $ 
%\clearpage
%--------------------------------------------------------------
\section{Interaction électrostatique}
\subsection{La charge électrique}
La matière est \textbf{électriquement neutre}. Cependant, elle est constituée 
de particules dont certaines sont électriquement chargées (protons et 
électrons). 
\begin{aConnaitre}
  \begin{itemize}
    \item La \textbf{charge électrique} est une caractéristique possédée 
    par certaines particules. La charge, notée $ q $, s'exprime en coulomb 
    (noté C). 
  \end{itemize}
\end{aConnaitre}
%suite autre page
\begin{aConnaitre}
  \begin{itemize}
    \item On distingue deux catégories : les \textbf{charges positives}  et 
    les \textbf{charges négatives}. 
    \item La \textbf{charge élémentaire}, notée $e$, correspond à la plus 
    petite valeur de charge électrique portée par une particule. $ e = 
    \qty{1,6 E-19}{C} $.
  \end{itemize}
\end{aConnaitre}
%	
\begin{center}
  \begin{tabular}{c*{2}{c}}
    \textbf{Particules} & masse (en $ kg $) & charge (en $ C $)  \\
    \toprule
    {\textbf{électron}} & \num{9,1 E-31}  &  \num{-1,6 E-19}  \\
    \midrule
    {\textbf{proton}}   &  \num{1,67 E-27}  &  + \num {1,6 E-19}  \\
    \midrule  
    {\textbf{neutron}}   & \num{ 1,67 E-27}  &  0  \\
    \bottomrule
  \end{tabular}
\end{center}
%
\subsection{Électrisation d'un corps}
\begin{aConnaitre}
  Un corps peut être électrisé par :
  \begin{itemize}
    \item \textbf{Frottement} : gain ou perte d'électron lors du frottement.
    \item \textbf{Contact} : il y a transfert d'électrons par contact d'un 
    corps chargé à un autre.
    \item \textbf{Influence} : un corps chargé électriquement engendre, 
    \emph{à distance}, un \emph{ déplacement de charge} à la surface d'un  
    conducteur placé à proximité.
  \end{itemize}
\end{aConnaitre}
%
\begin{minipage} {0.4\linewidth}
  \begin{ex}{}
    Déviation d'un filet d'eau par influence électrostatique 
  \end{ex}
\end{minipage}
\begin{minipage}{.55\linewidth}	
  \centering
  \includegraphics [scale=0.35]{./img/electrisation_influence.png}
\end{minipage}
%
\subsection{Force électrostatique : la loi de Coulomb}
L'influence électrostatique met en évidence l'interaction entre les charges qui 
peut se modéliser par la loi de Coulomb. Une interaction électrostatique peut 
être attractive ou répulsive.
%
\begin{aConnaitre}
  L’interaction  électrostatique  entre  deux  corps ponctuels 
  \textbf{\textcolor{blue}{A}} et \textbf{\textcolor{blue}{B}}, de  charges 
  respectives $q_A$ et $q_B$, séparés par une distance d est modélisée par 
  des \textbf{forces électrostatiques} \textcolor{red}{$ \vv{F_{A/B}}  $} et 
  \textcolor{red}{$ \vv{F_{B/A}}  $}. 
  
  L'expression de la force \textcolor{red}{$ \vv{F_{B/A}}  $} est définie par 
  la \textbf{loi de Coulomb} : 
  \begin{center}
    \formule{$ \vv{F_{B/A}} = k \times \dfrac{{q_A} \times {q_B}}{d^{2}}  
      \textcolor{green!40!black}{\vec{u}} $}
    {$ F $ / N \\
      $ q $ / C \\
      $ d $ / m
    }
    $ k = \qty{9,00 E9}{\N\square\m\per\square\coulomb} $
  \end{center}	
\end{aConnaitre}
\textbf{Représentation des forces électrostatiques selon des charges} :
% 
\begin{figure}[ht]
  \begin{minipage}[b]{0.4\linewidth}
    \centering
    $q_A$ et $q_B$ de signe opposé :
    \vskip 0.5cm
    \input{ ./figures/coulomb_attractive.tex }
    \caption{Interaction attractive}
  \end{minipage}
  \begin{minipage}[b]{0.55\linewidth}
    \centering
    $q_A$ et $q_B$ de même signe:
    \vskip 0.5cm
    \input{ ./figures/coulomb_respulsive.tex}
    \caption{Interaction répulsive}
  \end{minipage}
\end{figure}
% -------------------------------------------------------------------------------
\section{Champs et lignes de champs}
\subsection{Généralités}
Un   objet, de par ses propriétés   physiques (masse,  charge,  ...) modifie  
les  propriétés  de l’espace: il en résulte un champ autour de lui. Des objets 
aux propriétés physiques appropriées subissent une force lorsqu’ils sont placés 
dans cette \textbf{région d’influence}.

\begin{aConnaitre}
  Un champ représente \textbf{la cartographie d’une grandeur physique dans l’espace}. On
  définit deux types champs:
  \begin{itemize}
    \item Les  \textbf{champs  scalaires} correspondant  à  une grandeur 
    scalaire.
    \item Les \textbf{champs vectoriels} correspondant à une grandeur 
    vectorielle.
  \end{itemize}
\end{aConnaitre}
%
\begin{ex}{}
  Le  champ  de vitesse  du  vent  est  un  champ vectoriel et celui de la 
  température est un champ scalaire.
  \begin{center}
    \includegraphics[scale=0.5]{./img/exempleChamps}
  \end{center}
\end{ex}
%
\begin{aConnaitre}
  \begin{itemize}
    \item La représentation du champ vectoriel se fait par l'intermédiaire 
    de \textbf{lignes de champ} qui donne la direction et le sens aux 
    vecteurs champs.  
    \item Une  \textcolor{red}{\textbf{ligne  de  champ  vectoriel}} est  
    une  \textbf{ligne tangente}  en  chacun  de  ses  points au 
    \textcolor{blue}{\textbf{vecteur champ}}. Elle est orientée par une 
    flèche dans le sens du vecteur.
  \end{itemize}
\end{aConnaitre}
\begin{figure}[ht]
  \centering
  \input{ ./figures/ligne_champ.tex }
  \caption{Représentation d'une ligne de champ}
  \label{ligneChamp}
\end{figure}	
%
\subsection{Champ de gravitation}
Un \textbf{corps massif}, de masse $ \symbf{M} $, crée autour de lui un champs de 
gravitation, noté $\vv{\mathscr{G}}$. 

Un \textcolor{blue}{objet A}, de masse $ \symbf{m} $, placé à son voisinage, subit 
une \textbf{force d'attraction gravitationnelle} : 
\[
  \vv{F_{g}} = m \times \vv{\mathscr{G}} 
\]
\enlargethispage{.5cm}
%
\begin{figure}[ht]
  \centering
  \input{ ./figures/champ_gravitation.tex }
  \caption{Champs de gravitation créé par un astre de centre C et de 
    masse \textbf{M}}
\end{figure}
%
\begin{aConnaitre}
  Il règne un \textbf{champ de gravitation} $ \vv{\mathscr{G}} $ en un
\textcolor{blue}{point A} de l'espace si une \textbf{particule} de masse $ \symbf{m} $
placée en ce point est soumise à une force de gravitation $ \vv{F_{g}} $ .
  
  Expression du champ de gravitation $ \vv{\mathscr{G}} $ :
  \begin{center}
    \formule{$ \vv{\mathscr{G}} = - G \times \dfrac{M}{d^{2}} \times  
      \textcolor{green!40!black}{\vec{u_r}} $}
    {$\mathscr{G}$ / \si{\N\per\kg} \\
      $M$ / kg\\
      $ d $ / m
    }	
  \end{center}
\end{aConnaitre}
%
\begin{demo}
  La force appliquée à l'objet de masse m est $ \vv{F_{g}} = m \times 
  \vv{\mathscr{G}} $.
  
  L'autre expression qui s'exerce sur cet objet est 	$ \vv{F_{g}} = - G 
  \times \dfrac{{m} \times {M}}{d^{2}} \textcolor{green!40!black}{\vec{u_r}} 
  $,
  
  or, $ \vv{\mathscr{G}} = \dfrac{\vv{F_{g}}}{m} $ ;
  
  Donc le champ engendré par une masse \textit{\textbf{M}} est $ 
  \vv{\mathscr{G}} = - G \times \dfrac{M}{d^{2}} 
  \textcolor{green!40!black}{\vec{u_r}} $ .
\end{demo}
%
\begin{figure}[ht]
  \centering
  \input{ ./figures/lignes_champ_gravit.tex }
  \caption{Représentation des lignes du champ gravitationnel}
\end{figure}

\textit{\textbf{\textbf{Remarque }}: Source du champ de gravitation $ \longrightarrow $
  corps massique de centre C et de masse M.}
%
\subsection{Champs de pesanteur}
\begin{aConnaitre}
  \begin{listeAC}
    \item Au niveau du sol de la Terre, un objet est soumis à son poids 
    \begin{center}
      \cadre{$ \vv{P} = m \times \vv{g} $}
    \end{center}
    \item $ \vv{g} $ est le \emph{champs de pesanteur}.
    \begin{itemize}[label=\ding{217}]
      \item Vertical.
      \item Vers le centre de la Terre.
      \item Localement considéré comme uniforme.
    \end{itemize}
    \item En première approximation, la valeur du champ de pesanteur 
    est égal à la valeur du champs gravitationnel à la surface de la
    planète.
    \[
      g = \mathscr{G}(R_{\text{planète}}) = G \times 
      \dfrac{M_{\text{planète}}}{R_{\text{planète}}^{2}}
    \]
  \end{listeAC}
\end{aConnaitre}
%
% \clearpage
\subsection{Champ électrostatique}
La présence d’une particule de charge  $\symbf {q_{A}} $ modifie les propriétés 
physiques de l’espace autour de lui. Un autre objet de charge $\symbf q_{B} $ 
plongé dans ce champ crée par $ \symbf q_A $, noté $ \vv{E} $, subit une 
\textbf{force électrostatique} : 
\[
  \vv{F_{e}} =  q_{B}  \times \vv{E} 
\]
\begin{aConnaitre}
  Il règne un \textbf{champ électrostatique} $ \vv{E} $ en un \textcolor{blue}{point B} de
  l'espace si une \textbf{particule} de charge électrique $ \symbf{q_B} $ placée en ce point
  est soumise à une force électrostatique $ \vv{F_{e}} $ .
  
  Expression du champ électrostatique $ \vv{E} $ :
  \begin{center}
    \formule{$ \vv{E} =  k \times \dfrac{q_{A}}{d^{2}} 		
      \textcolor{green!40!black}{\vec{u}} $	
    }
    {$E$ / \si{\N\per\coulomb} (ou \si{\V\per\m})\\
      $q_{A}$ / C\\
      $ d $ / m 
    }
  \end{center}
  \[
    k = \qty{9,00 E9}{\N\square\m\per\square\coulomb}
  \]
\end{aConnaitre}
%
\begin{figure}[ht]
  \centering
  \begin{minipage}[b]{0.45\linewidth}
    \centering
    $q_A$ et $q_B$ de signe opposé :
    \vskip 0.5cm
    \input{ ./figures/lignes_champ_elec_attract.tex}
    \caption{Interaction attractive}
  \end{minipage}
  % \hspace{1cm}
  \begin{minipage}[b]{0.45\linewidth}
    \centering
    $q_A$ et $q_B$ de même signe:
    \vskip 0.5cm
    \input{ ./figures/lignes_champ_elec_repuls.tex }
    \caption{Interaction répulsive}
  \end{minipage}
\end{figure}
%
\textit{\textbf{Remarques}}:
  \begin{itemize}
    \item Source du champ électrostatique $ \longrightarrow $ particule de charge
    $ {q_A} $ de signe positif ou négatif.
  \item Plus le champ est intense, plus les lignes de champ de resserrées.
  \end{itemize}
%-----------------------------------------------------------------
\section{Bilan : Analogies}
%\renewcommand{\arraystretch}{2}
% \setlength{\extrarowheight}{2cm}
% \begin{tabular}{
%   |>{\centering\arraybackslash}m{2.3cm}
%   |>{\centering\arraybackslash}p{5.8cm}
%   |>{\centering\arraybackslash}p{5.8cm}|
%   }
%   \hline\rowcolor{gray!40} 
%   &  \textbf{Champ de GRAVITATION} 
%   & \textbf{Champ ÉLECTROSTATIQUE} 
%   \\\hline  
%   Corps source du champ 
%   &  \rule{0pt}{2.5cm}
%   &  
%   \\\hline 
%   Système placé dans le champ (particule test) 
%   &  \rule{0pt}{2.5cm}
%   & 
%   \\\hline 
%   Force subie par le système placé dans le champ dû au corps source  
%   &  \rule{0pt}{2.5cm}
%   &
%   \\\hline 
%   Autre expression de la force 
%   &  \rule{0pt}{2.5cm}
%   & 
%   \\\hline 
%   Expression du champ obtenue par identification entre les deux 
%   expressions des forces 
%   &  \rule{0pt}{2.5cm}
%   & 
%   \\\hline
%   Cas particulier 
%   & À la surface de la Terre \newline \rule{0pt}{2.5cm}
%   & Dans un condensateur plan  \newline \rule{0pt}{2.5cm}
%   \\\hline
% \end{tabular}

\begin{tblr}{
    hlines, vlines,
    hline{2} = {1}{-}{solid},
    hline{2} = {2}{-}{solid},
    columns = {c},
    rows = {m, 3cm},
    row{1} = {font=\bfseries, 1cm},
    cell{Z}{2,3} = {valign=h},
    colspec={XX[3]X[3]},
  }

  & Champ de gravitation
  & Champ électrique \\
  Corps source du champ  &  & \\
  %
  Particule test (système placé dans le champ)  &  & \\
  %
  Force subie par le système dans le champ dû au corps source  &  & \\
  %
  Autre expression de la force  &  & \\
  %
  Expression du champ obtenu par identification entre les deux expression de la force  &  & \\
  %
  Cas particulier  & À la surface de la Terre   &  Dans un condensateur plan \\
\end{tblr}
\end{document}
