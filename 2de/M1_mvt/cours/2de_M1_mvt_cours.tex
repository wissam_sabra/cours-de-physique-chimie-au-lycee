\documentclass[french, a4paper]{article}
\input{../../../preambules/mes_packages_lua}
\input{../../../preambules/style_cours}
\fancyhead[L]{\textbf{Chapitre M1.} \textcolor{gray!60}{Seconde}}
\fancyhead[R]{\themeMvt}
%--------------------------------------------------
\begin{document}
\titre{Décrire un mouvement}
\section{système étudié}
\begin{itemize}
  \item Le système est l'objet dont on étudie le mouvement.
  \item En seconde, on \emph{modélise} le système par un
  \emph{point matériel} (le plus souvent son centre géométrique).
  \item Cette modélisation engendre une perte d'information.
  \item La modélisation est d'autant plus pertinente que les dimensions du 
  système sont petites devant celles du mouvement.
\end{itemize}
\begin{ex}{Modélisation}
  Lorsqu'on modélise cette roue par son centre O, on perd l'information 
  concernant sa rotation.
  \begin{minipage}{.85\linewidth}
    \centering
    \includegraphics[scale=.2]{./img/mouvement_roue}
  \end{minipage}
  \begin{minipage}{.1\linewidth}
    \centering
    \qrcode[height=1cm]{https://www.youtube.com/watch?v=9Q20hxTLHhE}
  \end{minipage}
\end{ex}
\section{Référentiel}
\begin{aConnaitre}
  \begin{listeAC}
    \item Le mouvement d'un système est \emph{relatif}, c'est à dire 
    qu'il dépend du référentiel d'étude.
    \item Un référentiel est un corps de référence, considéré comme 
    immobile, auquel on associe un \emph{repère d'espace} et une 
    \emph{horloge}.
    \begin{dinglist}{217}
      \item Le repère d'espace permet de repérer la position du système.
      \item L'orloge permet de relier chaque position à une date $ t $.
    \end{dinglist}
  \end{listeAC}
\end{aConnaitre}
\begin{table}[ht]
  \centering
  \begin{tabular}{
    |>{\centering\bfseries}m{2cm}|
    *{3}{>{\centering\arraybackslash}m{3.8cm}|}}
    \hline
    Référentiels usuels
    & terrestre
    & géocentrique
    & héliocentrique \\
    \hline
    Repère d'espace associé 
    & Repère fixe par rapport au sol.
    & Repère ayant le centre de la Terre pour origine et dont les 
      axes pointent vers trois étoiles fixes.
    & Repère ayant le centre du Soleil pour origine et dont les 
      axes pointent vers trois étoiles fixes. \\
    \hline
    Mouvement de la Terre
    & Immobile
    & Rotation sur elle même.
    & Rotation sur elle même et révolution autour du Soleil. \\
    \hline
  \end{tabular}
\end{table}

\parbox
{.45\linewidth}{
  \begin{ex}{Deux observateurs différents}
    L'observateur est en mouvement dans le référentiel lié au train et 
    immobile dans celui lié au quai de la gare.
  \end{ex}
}
\parbox{.45\linewidth}{
  \centering
  \includegraphics[scale= 0.1]{./img/train_ref}
}
% ------------------------------------
\section{Mouvement d'un système}
\begin{aConnaitre}
  \begin{listeAC}
    \item Le mouvement d'un système est définit par \emph{deux adjectifs} 
    : le premier concerne \emph{la trajectoire} et 
    le deuxième \emph{l'évolution de la vitesse}.
    \item La \emph{trajectoire} est l'ensemble des positions occupées par 
    le système au 
    cours du mouvement. On la représente par une ligne.
    \item La \emph{vitesse moyenne} est le quotient de la distance
    entre deux points, sur la durée nécessaire pour la parcourir.
    \begin{center}
      \cadre{$ v = \dfrac{d}{\Delta t} = \dfrac{MM'}{t'-t}$}
    \end{center}
  \end{listeAC}
\end{aConnaitre}
% 
\begin{figure}[ht]
  \input{./fig/carte_ment_traject.tex}
  % 
  \caption{Caractériser la trajectoire}
\end{figure}

\begin{figure}[ht]
  \input{./fig/carte_ment_vit.tex}
  % 
  \caption{Caractériser l'évolution de la vitesse}
\end{figure}
%----------------------------------------------------------------------
\section{Vecteurs déplacement et vecteur vitesse}
\begin{figure}[ht]
  \begin{minipage}{.5\linewidth}
    \begin{aConnaitre}
      \begin{listeAC}
        \item \emph{Le vecteur déplacement} entre deux points $M$ et 
        $M'$ 
        relie le point de départ au point d'arrivé $ \vv{MM'} $
        \item En pratique, on dispose souvent des \emph{coordonnées} 
        des 
        positions et on 
        calcule:
        \[
          \left\{
            \begin{aligned}
              &MM'_x = x_{M'} - x_M \\
              &MM'_y = y_{M'} - y_m
            \end{aligned}
          \right.
        \]
        \item Caractéristiques du vecteur:
        \begin{itemize}
          \item \emph{Direction} : $ (MM') $
          \item \emph{Sens} :  celui du mouvement.
          \item \emph{norme} : distance $ MM' $
        \end{itemize}
      \end{listeAC}
    \end{aConnaitre}
  \end{minipage}
  % 
  \begin{minipage}{0.42\linewidth}
    \centering
    \input{./fig/vect_deplacement.tex}
    \caption{Coordonnées du vecteur déplacement }
  \end{minipage}
\end{figure}
\begin{aConnaitre}
  \begin{listeAC}
    \item \emph{Le vecteur vitesse moyenne } entre deux positions
    est défini par:
    \begin{center}
      $\vv{v} = \dfrac{\vv{MM'}}{t' - t} $
    \end{center}
    % \item D'après cette définition il est \emph{colinéaire} au vecteur 
    % déplacement.
    \item En pratique, on utilise également les coordonnées des positions.
    \[ 
      \left\{ 
        \begin{aligned} 
          v_x &= \frac{x_M' - x_M}{t' - t} \\ 
          v_y &= \frac{y_M' - y_M}{t' - t}
        \end{aligned}
      \right.
    \]
    % \item On peut ensuite calculer la \emph{valeur de la vitesse } qui 
    % correspond à sa 
    % norme:
    % \begin{center}
    %   \cadre{$ v = \sqrt{v_x^2 + v_y^2} $}
    % \end{center}
  \end{listeAC}
\end{aConnaitre}
% ------------------------------------------------------------

\section{Vecteurs vitesse en un point et caractérisation du mouvement}
Au cours du mouvement, la vitesse et la direction de déplacement peuvent 
varier
et le vecteur vitesse moyenne n'indique pas ces informations.
\begin{aConnaitre}
  \begin{listeAC}
    \item Le vecteur vitesse en un point peut être \emph{approché} par le 
    vecteur vitesse moyenne entre deux dates rapprochées: en pratique on 
    prends \emph{deux points successifs}, notés $ M_i $ et $ M_{i+1} $.
    \begin{center}
      \cadre{$ \vv{v_i} = \dfrac{\vv{M_{i}M_{i+1}}}{t_{i+1} - t_{i}} $}
    \end{center}
    \item Et avec les coordonnées, on écrit:
    \[ 
      \left\{ 
        \begin{aligned} 
          v_{xi} &= \frac{x_{i+1} - x_i}{t_{i+1} - t_{i}} \\ 
          v_{yi} &= \frac{y_{i+1} - y_i}{t_{i+1} - t_{i}}
        \end{aligned}
      \right.
    \]
  \end{listeAC}
\end{aConnaitre}
%

Les variations du vecteurs vitesse au cours du mouvement permet de le 
caractériser.

Dans la figure \ref{fig:mvt}, la \emph{direction} du vecteur vitesse est 
constante au cours des mouvements, ce sont dont des mouvements rectilignes.
La variation de la \emph{norme} du vecteur vitesse permet de préciser si le 
mouvement est \emph{uniforme, accéléré ou ralenti}
\begin{figure}[ht]
  \centering
  \input{./fig/mvts_rect.tex}
  % 
  \caption{Mouvements rectilignes...}
  \label{fig:mvt}
\end{figure}

% \begin{figure}[ht]
%   \centering
\begin{center}
  \begin{minipage}[t]{.3\linewidth}
    \centering
    \qrcode{https://www.youtube.com/watch?v=ciEQANTBk-0}
    \captionof*{figure}{Comprendre le vecteur vitesse}
  \end{minipage}
  \begin{minipage}[t]{.3\linewidth}
    \centering
    \qrcode{https://www.youtube.com/watch?v=EAQyfzA4DQo}
    \captionof*{figure}{Pour s'entrainer}
  \end{minipage}
\end{center}
% \end{figure}
\end{document}
