(TeX-add-style-hook
 "2de_C12_cours"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "french")))
   (TeX-run-style-hooks
    "latex2e"
    "mes_packages"
    "style_cours"
    "article"
    "art10"))
 :latex)

