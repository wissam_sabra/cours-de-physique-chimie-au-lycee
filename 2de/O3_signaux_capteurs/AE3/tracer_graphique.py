import matplotlib.pyplot as plt

R = [...]
T = [...]

plt.figure()
plt.plot(..., ..., 'o:')
plt.grid()
plt.xlabel("...")
plt.ylabel("...")
plt.title("...")
plt.show()
