'''Programme de mesure de température avec une CTN  , une
résitance R de 5 kOhm et une microbit.
L’étalonnage de la CTN a été fait de manière empirique, il faudrait vérifier la
doc de ce modèle'''
#####################
#  Initialisation   #
#####################

# Bibliothèques
from microbit import *
from math import *

# desactive la matrice de LED pour libérer le pin3 et le pin4
display.off()

pinMesure = pin4
pinB = pin1
pinV = pin2
pinR = pin3
R2 = 5e3
#
Tmin = 50
Tmax = 60

##################################################
# Boucle infinie --> "True" est toujours vrai ;) #
##################################################

while True:
    ## Etape 1 : Mesure de la température ##
    # la microbit enregistre une valeur entre 0 et 1023
    valeur = pinMesure.read_analog()
    # 3.3 V correspond à 1023, on peut donc en déduire la tension
    U_R = valeur * 3.3 / 1023
    # Puis la résistance R_th
    RCTN = R2 * (3.3 / U_R - 1)
    # Relation touvée par modélisation de la courbe d'étalonnage
    temperature = -22.1 * log((RCTN *1e-3 - .516) / 28.2)

    #Affichage du résultat
    print("T = ", temperature, " C")

    ## Etape 2 : comportement des LED ##
    if temperature < Tmin:
        pinB.write_digital(1) # LED bleu allumée
        pinV.write_digital(0) # LED verte éteinte
        pinR.write_digital(0) # LED rouge éteinte
    elif temperature < Tmax:
        pinB.write_digital(0) # LED bleu éteinte
        pinV.write_digital(1) # LED verte allumée
        pinR.write_digital(0) # LED rouge éteinte
    else :
        pinB.write_digital(0) # LED bleu éteinte
        pinV.write_digital(0) # LED verte éteinte
        pinR.write_digital(1) # LED rouge allumée
    sleep(500) # pause de 500 ms entre chaque mesure
