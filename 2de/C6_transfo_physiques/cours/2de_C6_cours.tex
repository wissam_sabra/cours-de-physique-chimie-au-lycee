\documentclass[french]{article}
\input{ ../../../preambules/mes_packages_lua.tex}
\input{ ../../../preambules/style_cours.tex }
\fancyhead[L]{\textbf{Chapitre C6.} \textcolor{gray!60}{Seconde}}
\fancyhead[R]{\themeMatiere}
% -------------------------------------------------------------------
\begin{document}
\titre{Les transformations physiques}
%-------------------------------------------------------------------
\section{Changements d'états de la matière}
\subsection{Modélisation macroscopique}
\begin{aConnaitre}
  \begin{listeAC}
    \item Une transformation physique d'un système est son passage d'un état
    physique à un autre.
    \item Dans la vie courante, on peut citer \emph{la liquéfaction} de l'eau
    sur une bouteille froide, ou encore la vaporisation de l'eau quand elle
    atteint \qty{100}{\degreeCelsius}.
  \end{listeAC}
\end{aConnaitre}
\begin{center}
  \begin{tikzpicture}[scale=.6]
    % \draw [help lines] (0,0) grid (15,10); 
    \node[draw, rectangle,rounded corners=3pt,fill=gray!50] (S) at (8,7) 
    {Solide};
    \node[draw, rectangle,rounded corners=3pt,fill=gray!50] (L) at (3,2) 
    {Liquide};
    \node[draw, rectangle,rounded corners=3pt,fill=gray!50] (G) at (12.5,2) 
    {Gaz};
    % 
    \draw[thick, <-,>=latex, color=blue] (L.north east) -- (S.south)
    node [below, midway, sloped] {fusion}
    ; 
    \draw[thick, ->,>=latex, color = red] (L.north) --
    (S.south west) node [above, midway, sloped] {solidification}
    ;
    % 
    \draw[thick, ->,>=latex, color=blue] (L.east) --(G.west)
    node [above, midway] {vaporisation}
    ; 
    \draw[thick, <-,>=latex, color = red] (L.-19) -- (G.south west) node 
    [below, midway] {liquéfaction}
    ; %(L.-20) permet d'avoir une ligne horizontale car cadre de G et L pas 
    % même hauteur
    \draw[thick, <-,>=latex, color=blue] (G.north west) -- (S.south) node 
    [below, midway, sloped] {sublimation}
    ;
    \draw[thick, <-,>=latex, color = red] (S.-30) -- (G.north) node [above, 
    midway, sloped] {condensation} ;
  \end{tikzpicture}
\end{center}
%
\subsection{Modélisation microscopique}
\begin{aConnaitre}
  \begin{listeAC}
    \item À l'état solide, les entités chimiques sont \emph{en contact} les unes
    avec les autres, sans mouvement les unes par rapport aux autres.
    \item À l'état liquide, les entités sont aussi en contact, mais également \emph{en
    mouvement constant}.
    \item À l'état gazeux, les entités sont \emph{très éloignées} les unes des
    autres et en mouvement constant.
  \end{listeAC}
\end{aConnaitre}
%
\begin{figure}[ht]
  \centering
  \includegraphics[scale=.1]{sol_liq_gaz}

  \textit{Source : lelivrescolaire.fr}
  \caption{les trois états de la matière}
\end{figure}
\subsection{Écriture symbolique des changements d'états}
\begin{aConnaitre}
  \begin{listeAC}
    \item Le changement d'état d'un corps pur est modélisé par une réaction
    dont l'équation utilise le même formalisme que les équations chimiques.
    \item \emph{L'état  physique} est indiqué par une lettre entre parenthèse:
    (s), (l) ou (g).
    \item Dans une transformation physique, il n'y a pas d'espèces chimiques
    consommées ou formées.
  \end{listeAC}
\end{aConnaitre}
\begin{ex}{Équation modélisant l'évaporation de l'eau.}
  \centering
  \ch{H2O(l) -> H2O(g)}
\end{ex}
%
\begin{aConnaitre}
  \textbf{Fusion ou dissolution?}
  \begin{listeAC}
    \item Lors d'une fusion, il y a \emph{passage de l'état solide à l'état
      liquide}.
    \item La \emph{dissolution} consiste à mélanger une espèce chimique avec un
    solvant (souvent l'eau).
    \item Lorsqu'une espèce est dissoute dans l'eau, on dit qu'elle est
    \emph{solubilisée} dans l'eau et on note (aq) dans l'équation modélisant la dissolution.
  \end{listeAC}
\end{aConnaitre}
\begin{multicols}{2}
  \begin{ex}{Dissolution du glucose dans l'eau:}
  \centering
  \ch{C6H12O6 (s) -> C6H12O6(aq)}
\end{ex}
%
\begin{ex}{Dissolution du chlorure de sodium dans l'eau}
  \centering
  \ch{NaCl(s) -> Na^+ (aq) + Cl^- (aq)}
\end{ex}
%
\end{multicols}

\section{Échange d'énergie par transferts thermique}
\begin{aConnaitre}
  \begin{listeAC}
    \item Le \emph{transfert thermique}, est un mode de transfert
    d'énergie entre deux corps.
    \item On note $Q$ l'énergie transférée par transfert thermique.
    \item Lorsqu'un système reçoit de l'énergie, $Q$ et positif et on peut
    observer une augmentation de la température et / ou un changement d'état.
    \item Lorsqu'un système cède de l'énergie, on peut observer une diminution
    de la température et / ou un changement d'état.
  \end{listeAC}
\end{aConnaitre}
%
\subsection{Transformations endothermiques et exothermiques}
\begin{aConnaitre}
  \begin{listeAC}
    \item Une transformation est \emph{\textcolor{red}{exothermique}} si elle
    libère de l'énergie que le système cède à l'extérieur sous forme de
    transfert thermique.
    \item Une transformation est \emph{\textcolor{blue}{endothermique}} si elle nécessite un apport
    d'énergie, que le système reçoit de l'extérieur sous forme de transfert thermique.
  \end{listeAC}
\end{aConnaitre}
%
\begin{figure}[ht]
  \centering
  \begin{subfigure}{.45\linewidth}
    \centering
    \begin{tikzpicture}
      \node[ rounded corners= 2pt, fill=yellow!50] (ext) at (0,0) {Extérieur};
      \node[draw, ellipse] (syst) at (4,0) {Système};
      \draw[->, ultra thick, red] (syst) -- (ext) node[midway, above] {$Q < 0$};
    \end{tikzpicture}
    \subcaption{Le système cède de l'énergie}
  \end{subfigure}
  \begin{subfigure}{.45\linewidth}
    \centering
    \begin{tikzpicture}
      \node[ rounded corners= 2pt, fill=yellow!50] (ext) at (0,0) {Extérieur};
      \node[draw, ellipse] (syst) at (4,0) {Système};
      \draw[<-, ultra thick, blue] (syst) -- (ext) node[midway, above] {$Q > 0$};
    \end{tikzpicture}
    \subcaption{Le système gagne de l'énergie}
  \end{subfigure}
  \caption{transferts thermiques}
\end{figure}
\subsection{Énergie thermique et changement d'état}
\begin{aConnaitre}
  \begin{listeAC}
    \item Lors du changement d'état d'un corps pur, on observe un \emph{palier
      de température.}
    \item À la température de changement d'état, l'énergie transférée par transfert
    thermique, ne sert plus à faire varier la température, mais \emph{uniquement à
      réaliser la transformation}.
    \item L'énergie échangée par transfert thermique, notée $Q$, lors d'un
    changement d'état est proportionnelle à la masse $m$ du système qui subit la transformation.
  \end{listeAC}
  \formule{$Q = m \times L$}{ $Q$ : \si{\joule} \\ $m$ : kg \\ L : \si{\joule\per\kg}}
\end{aConnaitre}
%
\enlargethispage{1cm}


\end{document}
