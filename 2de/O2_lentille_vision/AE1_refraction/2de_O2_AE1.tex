\documentclass[french]{exam}
\input{../../../preambules/mes_packages_lua}
\input{../../../preambules/style_act}
\header{\textbf{Chapitre O2.} \textcolor{gray!60}{Seconde}}
{}
{\themeOndes}
% -------------------------------------------------------------------------------
\begin{document}
\titre{\textbf{AE1.} Réfraction de la lumière}
%
\begin{competences}
  \comp{RÉA}{Réaliser des mesures avec précision}
  \comp{RÉA}{Utiliser l'outil informatique (tracer un graphique)}
  \comp{VAL}{Confronter les résultats expérimentaux à un modèle}
  \comp{COM}{Présenter la démarche de manière claire}
\end{competences}
%
\begin{minipage}{.7\linewidth}
  \begin{contexte}
  Aniline, le chat de votre professeur semble intéressée par Léon (le poisson). Mais pour
  l'attraper, ce n'est pas si simple!
  
  \textbf{Objectifs:}
  \begin{itemize}
    \item Découvrir et tester les lois de Snell-Descartes.
    \item Utiliser les lois de Snell-Descartes pour déterminer le sens de déviation d’un
    rayon lumineux.
  \end{itemize}
\end{contexte}
\end{minipage}
%
\begin{minipage}{.25\linewidth}
  \centering
  \includegraphics[scale=.7]{img/ani.png}
\end{minipage}
%
% -------------------------------------------------------------------------------
%
\section{\infos}
\begin{docs}
  \docL[sidebyside, lefthand ratio=.4]{Schéma de l'expérience et vocabulaire}{hemicylindre}
  \centering
  \begin{tikzpicture}[scale=1] 
    \newcommand{\drawarc}[7] { 
      \draw (#1,#2) arc(#3:#4:#5); 
      \draw({(#3+#4)/2}:#5) node[#6]{#7}; 
    } 
    % simple flèche 
    \tikzstyle simple=[postaction={decorate,decoration={markings, mark=at position .5 with
        {\arrow[scale=1,>=stealth]{>}}}}]
    % simple flèches inverse 
    \tikzstyle simpleinv=[postaction={decorate,decoration={markings, mark=at position .5
        with {\arrow[scale=1,>=stealth]{<}}}}]
    %
    \newcommand{\rayon}{2}; %rayon du disque gradué 
    \newcommand{\angleI}{40}; %angle d'incidence  
    \newcommand{\angleR}{25.4}; %angle de réfraction 
    %
    \draw (0,0) circle (\rayon); 
    \foreach \y in {10,20,...,360} \draw({\rayon*cos(\y)},{\rayon*sin(\y)})--++(\y:-0.2);
    \foreach \y in {0,90,...,270} \draw[]({\rayon*cos(\y)},{\rayon*sin(\y)})--++(\y:-0.3); 
    \draw[dotted](-\rayon,0)--++(2*\rayon,0);
    %
    \draw [color=gray,fill=yellow!20] (0,0)
    --++({\rayon-0.5},0)arc(0:-180:{\rayon-0.5})--cycle;%Hémicylindre
    %
    \draw[dotted](0,\rayon)--++(0,-2*\rayon); 
    \draw [color=red,simpleinv,line width=1pt] (0,0) --++ (180-\angleI:\rayon+1); 
    \node at (\rayon+0.35,0){$-90^{\circ}$}; 
    \node at (-\rayon-0.3,0){$90^{\circ}$}; 
    \node at (0,\rayon+0.3){$0^{\circ}$}; 
    \node at (0,-\rayon-0.3){$0^{\circ}$}; 
    \draw [color=red,simple,line width=1pt] (0,0) --++ (\angleI:\rayon+1); 
    \draw [color=red,simple,line width=1pt] (0,0) --++ (-90+\angleR:\rayon+1); 
    \drawarc{0}{1}{90}{90+\angleI}{1.2}{above}{$i_1$};%angle d'incidence 
    \drawarc{0}{-1}{-90}{\angleR-90}{1}{below}{$i_2$};%angle de réfraction 
    \drawarc{0}{.8}{90}{90-\angleI}{1}{above}{$r$};%angle de réflexion
    \draw (0,0) node [below left] {$I$};
  \end{tikzpicture}
  %
  \tcblower
  \begin{itemize}
    \item \textbf{Dioptre:} séparation entre deux milieux transparents.
    \item \textbf{Point d'incidente I :} point imaginaire d'intersection entre le rayon incident
    et le dioptre.
    \item \textbf{Normale:} droite imaginaire perpendiculaire au dioptre, passant par le
    point d'incidence (se propage dans le milieu 1)
    \item \textbf{Rayon incident:} rayon lumineux arrivant sur le dioptre.
    \item \textbf{Rayon réfléchi:} rayon lumineux renvoyé dans le même milieu que le rayon
    incident.
    \item \textbf{Rayon réfracté:} rayon se propageant dans le milieu 2.
    \item \textbf{Angle d'incidence:} angle entre le rayon incident
    \emph{\textcolor{red}{et la normale}}
    \item  \textbf{Angle de réfraction :} angle entre le rayon réfracté
    \emph{\textcolor{red}{et la normale}}
  \end{itemize}
  %
    \docL[]{Indice de réfraction}{indice}
    Un milieu transparent est caractérisé par son \emph{indice de réfraction.}

    L'indice de réfraction, noté $n$, est \textcolor{red}{\emph{le quotient de la vitesse de
        propagation dans le vide par la vitesse de propagation de la lumière dans le
        milieu.}}:
    \[
      n_i = \dfrac{c}{v_i}
    \]
    On donne quelques exemples ci-dessous:
    % 
    \tcblower
    \centering
    \begin{tblr}{
        hlines, vlines,
        row{1} = {font=\bfseries, bg=couleurDoc!50},
        column{1} = {font=\bfseries, bg=couleurDoc!50, wd=.2\linewidth},
        cell{3}{2-7} = {mode=math},
        rows = {c, m},
      }
      Milieu
      & Air
      & Éthanol
      & Dimant
      & Eau
      & Plexiglas
      & Glycérine \\
      % 
      État physique à température ambiante
      & Gaz
      & Liquide
      & Solide
      & Liquide
      & Solide
      & Liquide \\
      % 
      Indice $\bm{n}$
      & 1,00
      & 1,32
      & 2,42 à 2,75
      & 1,33
      & 1,51
      & 0,47  \\
    \end{tblr}
  %
  \docL[]{Lois de Snell-Descartes}{snell}
  Lorsqu'un rayon passe d'un milieu transparent $n_1$ à un autre milieur $n_2$ :
  \[
    n_1 \times \sin(i_1) = n_2 \times \sin(i_2)
  \]
  avec $n_1$ et $n_2$ les indices de réfractions des milieux 1 et 2.
  %
  \docL[sidebyside]{Modélisation et exploitation d'un nuage de points}{modelisation}
  Une méthode expérimentale pour déterminer la relation mathématique entre deux grandeurs
  physiques est de \emph{mesurer plusieurs couples de valeurs et de tracer le graphique
    réprésentant l'évolution de l'une en fonction de l'autre.}

  Lorsque les points semblent \emph{alignés sur une droite passant par l'origine}, on
  considère que les deux grandeurs sont \emph{proportionnelles.}

  Dans ce cas on trace une \emph{courbe de tendance} de type linéaire, c'est à dire une
  droite passant par l'origine et dont le coefficient directeur la fait passer le plus
  proche possible de l'ensemble des points.
  \tcblower
  \begin{tikzpicture}
    \begin{axis}[
      % scale=1.2,
      width  = .8 \linewidth,
      height = 7.5cm,
      xmin = 0 ,% xmax = ,%
      ymin = 0, % ymax =,%
      xlabel = Grandeur 1 / unité ,%
      ylabel = Grandeur 2 / unité ,%
      grid = both,
      minor tick num=2,
      tickpos=left,
      minor grid style = {color=lightgray!20},
      major grid style = {color = lightgray},
      yticklabel style={
        /pgf/number format/.cd,
        set thousands separator={\,}, % ou 1000 sep = {\,}
        set decimal separator={,}, % ou use comma
        fixed
        /tikz/.cd % pour re
      },
      xticklabel style={
        /pgf/number format/.cd ,
        set decimal separator={,},
        set thousands separator={\,},
        fixed % pour que la virgule des nombres soit fixée
        /tikz/.cd
      }
      ]
      % \addplot{150 * \x};
      \addplot table [x = sin(i2), y = sin(i1), col sep = comma
      ]
      {resultats_possibles.csv};
      \addlegendentry{Modélisation : $y=1,5 \times x$}
    \end{axis}
  \end{tikzpicture}
\end{docs}
%
% -------------------------------------------------------------------------------
%
\section{\travail}

\subsection{Travail préliminaire}
\begin{enumerate}
  \item \rea Exprimer $\sin(i_1)$ en fonction de $\sin(i_2)$, $n_2$ et
  $n_1$. \refDoc{snell}
  \item \ana Représenter l'allure que doit avoir le graphique représentant $\sin(i_1)$ en
  fonction de $\sin(i_2)$ d'après cette loi.
  \item \ana Exprimer le coefficient directeur de ce graphique en fonction de $n_1$ et $n_2$.
  \item \ana Calculer sa valeur si :
  \begin{enumerate}
    \item le milieu 1 est de l’eau et le milieu 2 de l’air;
    \item le milieu 1 est de l’air et le milieu 2 du plexiglas.
  \end{enumerate}
\end{enumerate}
%
\subsection{Mise en œuvre du protocole}
\begin{enumerate}[resume]
  \item \rea Réaliser le montage correspondant au document \refDoc{hemicylindre}, et faire
  varier l'angle d'incidence et identifier la valeur pour laquelle le faisceau n'est pas
  dévié.
  \item \rea Choisir l'angle d'incidence de son groupe dans le tableau suivant et mesurer
  l'angle de réfraction correspondant. Recopier et compléter le tableau avec les résultats
  des autres groupes. \refDoc{hemicylindre}
\end{enumerate}
%
\begin{table}[ht]
  \centering
  \begin{tblr}{
      vlines, hlines,
      rows = {c,m},
      row{1} = {font=\bfseries, bg=couleurDoc!50},
    }
    \No de groupe
    & 1G
    & 2G
    & 3G
    & 1D
    & 2D
    & 3D \\
    %
    Angle d'incidence \qty{10}{\degree}
    & \qty{10}{\degree}
    & \qty{20}{\degree}
    & \qty{30}{\degree}
    & \qty{40}{\degree}
    & \qty{50}{\degree}
    & \qty{60}{\degree} \\
    Angle de réfraction & & & & & & \\ 
  \end{tblr}
  \caption{tableau de mesures expérimentales}
  \label{tab:mesures}
\end{table}
%
\subsection{Vérification de la loi de Snell-Descartes}
Pour vérifier que les résultats expérimentaux sont en accord avec la loi, on souhaite
tracer le graphique représentant $\sin(i_1)$ en fonction de $\sin(i_2)$.
\begin{enumerate}[resume]
  \item Ouvrir le fichier \fichier{O2\_AE1\_fichier\_a\_completer} et y entrer les
  valeurs.
\end{enumerate}
  %
\tipbox{
  En lisant attentivement la notice de libreOffice, et avec un peu d’astuce, on
  peut la consigne suivante peut être réalisée en \emph{trois coups de souris}.}
%
\begin{enumerate}[resume]
  \item Compléter dans le fichier les deux dernières colonnes. \emph{Une cellule permettant de
  calculer le sinus de $i_1$ est déjà complétée.}
\end{enumerate}

\warningbox{Les valeurs de la colonne la plus à gauche seront par défaut reportées en
  abscisses. Pour la consigne suivante, il faudra donc \textbf{les modifier en suivant les
    indications contenues dans la notices.} }

\begin{enumerate}[resume]
   \item Tracer le graphique représentant $\sin(i_1)$ en fonction de
   $\sin(i_2)$. Schématiser l’allure du graphique obtenu.
   \item Les points expérimentaux sont-ils en accord avec la loi de Snell-Descartes ?
   \item Ajouter Sur le graphique la courbe de tendance et son équation. Noter l’équation
   obtenue en conservant trois chiffres significatifs.
   \item Le coefficient directeur est-il en accord avec la loi de Snell-Descartes ?
\end{enumerate}

\subsection{Retour sur le poisson}
\begin{enumerate}[resume]
  \item \val Expliquer à l'aide de schéma où doit se placer le chat de votre professeur
  pour attrapper le poisson.
  \item \ana Le chat sur le dessin ci-dessous essaie d'attraper le poisson rouge.
  \begin{enumerate}
    \item Justifier que lorsqu’un rayon issus du poisson passe de l’eau à l’air,
    \textbf{il s’écarte de la normale.}
    \item  Expliquer quel poisson il risque d'attrapper à la place. Justifier la réponse en
    traçant un rayon lumineux issus du poisson.
  \end{enumerate}
\end{enumerate}
\begin{figure}[ht]
  \centering
  \includegraphics[scale=1]{ ./img/chat poissons.png }
\end{figure}

\end{document}
