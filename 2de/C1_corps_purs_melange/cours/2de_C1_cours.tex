\documentclass[french]{article}
\input{ ../../../preambules/mes_packages_lua}
\input{ ../../../preambules/style_cours}
%Entête ---
\fancyhead[L]{\textbf{Chapitre C1.} \textcolor{lightgray}{Seconde}}
\fancyhead[C]{}
\fancyhead[R]{\themeMatiere}

%------

\begin{document}
\titre{\textbf{C1.} Corps purs et mélanges}
\vspace{0.2cm}
%-----------------------------------
\section{Quelques définitions}
\begin{grandCadre}
  \begin{itemize}[label=\ding{46}, %leftmargin=*
    ]
    \item Une \textcolor{blue}{\textbf{espèce chimique}} correspond à un \textbf{ensemble
      d’\textcolor{yellow!55!red}{entités chimiques} identiques} (entités : atomes,
    molécules, ions).
    \item Un \textbf{corps pur} est constitué d’\textbf{une seule espèce chimique}. 
    \item Un \textbf{mélange} est constitué de \textbf{plusieurs espèces chimiques}.
    \item \underline{Mélange homogène} : mélange dont on ne distingue pas les différents
    constituants à l’œil nu (exemple: eau salée).
    \item \underline{Mélange hétérogène} : mélange dont on distingue les différents
    constituants à l’œil nu.
    \item Deux liquides sont \textbf{non-miscibles} s’ils forment un \textbf{mélange
      hétérogène}. Réciproquement.
  \end{itemize}
\end{grandCadre}
%
\begin{tcbraster}[raster columns=1]
    \begin{ex}[label=eauSalee, sidebyside, lefthand ratio=.7]{}
    L’eau salée est un mélange homogène, mais le jus d’orange avec pulpe forment un
    mélange hétérogène.
    
    L’eau et l’huile forment un mélange hétérogène : ils sont non-miscibles.
    \tcblower
    \includegraphics[scale =0.06]{./img/schemaNonMiscible}
  \end{ex}
\end{tcbraster}
%
\begin{tcbraster}[raster columns = 2, raster valign = top, raster halign=center]
  \begin{ex}{}
    \begin{tabular}{cc}
      \textcolor{blue}{\textbf{Espèce chimique}}
      & \textcolor{yellow!55!red}{\textbf{Entité chimique}}  \\\toprule
      \textcolor{blue}{Eau}
      & \textcolor{yellow!55!red}{Molécule d'eau} \\
      \midrule
      \textcolor{blue}{Fer métallique}
      & \textcolor{yellow!55!red}{Atome de fer}  \\
      \bottomrule
    \end{tabular}
  \end{ex}
%
  \begin{ex}{}
    Les eaux minérales ne sont pas des corps purs et contiennent de nombreux ions dissous.
    \begin{center}
      \includegraphics[scale =0.25]{./img/etiquetteEau}
    \end{center}
  \end{ex}
%
\end{tcbraster}
%-----------------------------------
\section{Identifications d’espèces chimiques par des mesures physiques}
\subsection{Masse volumique}
%
\begin{minipage}{.65\linewidth}
  \begin{aConnaitre}
    La \textbf{masse volumique} de l'espèce chimique (ou d'un mélange), notée $ \rho $
    (rhô), est définie comme étant le \textbf{quotient} de la masse de l'échantillon de
    matière de l'espèce chimique (ou d'un mélange) et de son volume :
    \begin{center}
      \formule{$ \rho  = \dfrac{m}{V} $}{
        $\rho$ en  \si{\g \per \L} \\
        $m$ en \si{g}\\
        $ V $ en \si{\l}
      }
    \end{center}
    Dans le SI, on exprime $ \rho $ en $ \si{\kg \per \m \cubed} $. En laboratoire, on
    utilise les $ \si{\g \per \L} $ ou $ \si{\g \per \mL} $
  \end{aConnaitre}
\end{minipage}
%
\begin{minipage}{0.29\linewidth}
  \centering
  \begin{ptMath}
    % \setlength{\baselineskip}{.5\baselineskip}
    \begin{center}
      $ \qty{1000}{L} = \qty{1}{\m\cubed} $
      \[ \qty{1}{L}  = \qty{1}{\deci\m\cubed} \]
      \[ \qty{1}{mL}  = \qty{1}{\centi\m\cubed} \]
      \[ \qty{1}{\deci\m\cubed} = \qty{1e3}{\centi\m\cubed} \]
    \end{center}
  \end{ptMath}
\end{minipage}

\begin{minipage}{.7\linewidth}
  \begin{ex}{} 
    \[ \rho_{eau} = \qty{1}{\kg\per\L} = \qty{1}{\g\per\mL} = \qty{1 000}{\g\per\L} = \qty{1
        000}{\kg\per\m \cubed} = \qty{1}{\g\per\centi\m\cubed} \]

    \begin{center}
      $ \rho_{cuivre}= \qty{8.96}{\g\per\centi\m\cubed}  $ 
      et 
      $ \rho_{air}= \qty{1.292}{\g\per\l}  $
    \end{center}
  \end{ex}
\end{minipage}
%
\begin{minipage}{.2\linewidth}
  \centering
  \qrcode{https://colibris.link/convertMasseVol}
  % url{https://colibris.link/convertMasseVol}
\end{minipage}
%
\subsection{Densité}
\begin{aConnaitre}
  La \textbf{densité}, notée \textit{d}, est le \textbf{quotient} de la masse volumique
  d'un corps et celle de l'eau pour un liquide ou un solide et de la masse volumique de
  l'air pour un gaz.  Il s'agit d'une grandeur sans unité.
  \begin{center}
    \begin{minipage}{0.3\linewidth}
      \centering
      Corps liquide ou solide :
      \cadre{$ d = \dfrac{\rho}{\rho_{eau}} $}
    \end{minipage}
    \hspace{1cm}
    \begin{minipage}{0.3\linewidth}
      \centering
      Corps gazeux :
      \cadre{$ d = \dfrac{\rho}{\rho_{air}} $}
    \end{minipage}
  \end{center}
\end{aConnaitre}

\begin{minipage}{0.75\linewidth}
  \begin{ex}{}
    La comparaison des masses volumiques (ou de la densité) de liquides non miscibles
    permet de prévoir la position relative de chaque liquide.
  \end{ex}
\end{minipage}
%
\begin{minipage}{0.2\linewidth}
  \begin{flushright}
    \includegraphics[scale=0.08]{./img/densite}
  \end{flushright}
\end{minipage}
% 
\subsection{Température de changement d’état}
\begin{aConnaitre}
  \begin{dinglist}{51}
    \item À pression constante, la température reste constante lors du changement d’état
    d’un corps pur : on observe un \textbf{palier de température}.
    \item En revanche, elle varie lors du changement d’état d’un mélange : aucun palier ne
    peut être observé.
    \item Une \textbf{espèce chimique} peut être identifiée par \textbf{ses températures
      de changement d’état} qui lui sont spécifiques.
  \end{dinglist}
\end{aConnaitre}
%
\begin{tcbraster}[raster columns = 2, raster valign = top]
  \begin{ex}{Courbe de refroidissement d'un corps pur et d'une mélange.}
    
    \centering
    \includegraphics[scale = 0.22]{./img/graphChangementEtat}
    \captionof*{figure}{Source: Magnard - 2de}
  \end{ex}
% 
  \begin{ex}{Températures de changement d'état de quelques espèces chimiques}
    \centering
    \begin{tabular}{cccc}
      \textcolor{blue}{\textbf{T / $ \si{\degreeCelsius} $}} & \textbf{eau} & \textbf{cuivre} & \textbf{mercure} \\
      \toprule
      $\color{blue} \theta_{fusion} $ & 0 & 1 084 & -39 \\
      \midrule
      $ \color{blue}\theta_{ebullition} $   & 100 & 2 562 & 357  \\
      \bottomrule
    \end{tabular}
  \end{ex}
\end{tcbraster}

Le \textbf{banc Kofler} permet de \textbf{repérer une température de fusion}. Un petit échantillon de \textbf{solide} déposé à droite du banc est poussé très lentement jusqu’à ce qu’il fonde. À cet endroit, on lit la température de fusion.

\begin{multicols}{2}
  \renewcommand{\columnseprule}{1pt}
  \renewcommand{\columnseprulecolor}{\color{gray!50!white}}
  \centering
  \includegraphics[scale = 0.15]{./img/bancKoflerPhoto}
  
  Photo d'un banc Kofler
  
  \columnbreak
  \includegraphics[scale = 0.2]{./img/bancKoflerSchema}
\end{multicols}

%-------------------------------------------------------------
\section{Identification d’espèces chimiques par des tests chimiques}

%\makegapedcells
%\setcellgapes{0.2cm}
\begin{tabular}{|>{\centering}m{1.5cm}|>{\centering}m{3cm}|>{\centering}m{2.7cm}|>{\centering}m{3cm}|>{\centering}m{2.7cm}|}
  \hline	
  \textbf{Espèce }
  &  \textbf{ion}
  & \textbf{\ch{CO2}}
  & \textbf{\ch{O2}}
  & \textbf{\ch{H2}} \tabularnewline\hline 
  Test  \textcolor{white}{}
  & \includegraphics[scale = 0.07]{./img/testIon}
  & \includegraphics[scale = 0.07]{./img/testEauChaux}
  & \includegraphics[scale = 0.07]{./img/testO2}
  & \includegraphics[scale = 0.07]{./img/testH2} \tabularnewline\hline
  Positif si ...
  & apparition d’un précipité
  & eau de chaux troublée
  &  incandescence amplifiée
  & détonation \tabularnewline\hline 
\end{tabular}
%---------------------------------------------------------------
\section{Identification d’espèces chimiques par CCM}
\begin{aConnaitre}
  \begin{dinglist}{51}
    \item La chromatographie sur couche mince (CCM) est une méthode de \textbf{séparation}
    et d’\textbf{identification d’espèces chimiques}.
    \item Les espèces chimiques migrent différentiellement suivant \textbf{leur affinité}
    pour la phase mobile (éluant) et la phase stationnaire (plaque de silice).
    \item \textbf{Plusieurs taches à différentes hauteurs caractérisent un mélange.}
    \item On identifie une espèce chimique \textbf{en la ­comparant à une espèce chimique
      de référence}.
  \end{dinglist}
\end{aConnaitre}
%
\begin{ex}{}
\begin{center}
  \includegraphics[scale = 0.14]{./img/chromatoCouleur}
\end{center}
\end{ex}
% \begin{minipage}{0.7\linewidth}
%   L’espèce chimique est identifiée par comparaison avec l’espèce chimique de référence ou
%   \textbf{en calculant le rapport frontal} $ R_f $ qui, pour un éluant et une phase
%   stationnaire donnés est une grandeur qui le caractérise.
%   \begin{center}
%     \cadre{$ R_f = \dfrac{h}{H} $}
%   \end{center}
% h : hauteur de la tache par rapport au front de l'éluant.

% H : hauteur séparant la ligne de dépôt et le front de l'éluant.
% \end{minipage}

% \begin{minipage}{0.19\linewidth}
%   \includegraphics[scale=0.21]{./img/schemaCCM}
% \end{minipage}

%-----------------------------------------------------------------
\section{Composition d'un mélange}
\subsection{Composition massique}
\begin{aConnaitre}
  \begin{dinglist}{51}
    \item La \textbf{composition massique d’un mélange}, par exemple celle d’un alliage
    est obtenue en déterminant le pourcentage en masse de ses constituants.
    \item La proportion en masse d’une espèce E dans un mélange est le quotient de la
    masse de cette espèce $ m_E $ sur la masse totale $ m_{tot} $ du mélange.
    \item Lorsque cette proportion en masse s'exprime en pourcentage : on parle de
    \textbf{pourcentage massique}.
  \end{dinglist}
  \begin{center}
    \formule{$ \dfrac{m_E}{m_{tot}}   $}{$m$ :même unité}
  \end{center}
\end{aConnaitre}
\begin{ex}{}
  L’acier et la fonte sont constitués notamment de fer et de carbone. On les différencie
  par leur teneur en carbone.
\end{ex}
\begin{center}
  \begin{tabular}{ccc}
    \textbf{alliage} & acier  & fonte \\
    \toprule
    \textbf{pourcentage massique} & $ 0,02\% $ à $ 2\% $  &  $ 2\% $ à $ 6.67\% $\\
    \bottomrule
  \end{tabular}
\end{center}

\subsection{Composition volumique}
%
\begin{aConnaitre}
  \begin{dinglist}{51}
    \item Pour un mélange gazeux comme l’air, on détermine sa \textbf{composition
      volumique}.
    \item La proportion en volume d’une espèce E dans un mélange est le quotient du volume
    de cette espèce $ V_E $ sur le volume total $ V_{tot} $ occupé par le mélange.
    \item Lorsque cette proportion en volume s'exprime en pourcentage : on parle de
    \textbf{pourcentage volumique}.
  \end{dinglist}
  \begin{center}
    \formule{$ \dfrac{V_E}{V_{tot}} $}{$V$ : même unité}
  \end{center}
\end{aConnaitre}
%
\begin{ex}{}
  La composition de l’air est donnée dans le tableau ci-dessous.
\end{ex}

%\makegapedcells
%\setcellgapes{0.5pt}
\begin{center}
  \begin{tabular}{cccc}
    & azote
    & dioxygène
    & argon \\ \toprule
    \textbf{pourcentage volumique}
    & $ 78\% $
    & $ 21\% $
    & $ 1\%$ \\ \midrule
    \textbf{proportion volumique}
    & $\dfrac{4}{5} $
    & $ \dfrac{1}{5} $
    & - \\ \bottomrule
  \end{tabular}
\end{center}
%
\begin{minipage}{0.2\linewidth}
  \includegraphics[scale=0.2]{./img/lavoisier}
\end{minipage}
%
\begin{minipage}{0.7\linewidth}
  L’air est un mélange dont Antoine de Lavoisier (1743-1794) a été le premier à établir la
  composition. Dispositif expérimental de Lavoisir :
  \begin{center}
    \includegraphics[scale=0.3]{./img/experienceLavoisier}
  \end{center}
\end{minipage}
\end{document}
