\documentclass[french]{article}

\input{ ../../../preambules/mes_packages_lua }
\input{ ../../../preambules/style_cours }
\fancyhead[L]{\textbf{Chapitre C2.} \textcolor{gray!70}{Seconde}}
\fancyhead[R]{\themeMatiere}
%
\DeclareSIUnit{\conc}{\g\per\litre}
% -------------------------------------------------------------------------------
%-------------------------------
\begin{document}
\titre{\textbf{C2.} Les solutions aqueuses}
%
\section{Quelques définitions}
\begin{aConnaitre}
  \begin{listeAC}
    \item Une \textbf{solution} est un mélange liquide homogène dans lequel une des
    espèces, généralement l'eau, est majoritaire.
    \item L'espèce majoritaire est appelée le \textbf{solvant}. Lorsque le solvant est
    l'eau, on parle d'une \textbf{solution aqueuse.}
    \item Les \textbf{solutés}, sont les autres espèces du mélange.
  \end{listeAC}
\end{aConnaitre}
%
\begin{minipage}{.65\linewidth}
  \begin{ex}{Le sérum physiologique}
    Le sérum physiologique est une solution aqueuse de chlorure de sodium: C'est de l'eau
    dans laquelle ont trouve des ions chlorures \ch{Cl-} et des ions sodium \ch{Na+}.
  \end{ex}
\end{minipage}
%
\begin{minipage}{.3\linewidth}
  \centering
  \includegraphics[scale=.3]{img/serum_phi}
\end{minipage}
%-----------------------------
\section{La concentration en masse}
\begin{aConnaitre}
  La concentration en masse, notée $ \gamma $ est\textbf{ la masse de soluté dissoute par
    unité de volume de solution}. elle s'exprime usuellement en \unit{\conc}, mais on peut
  l'exprimer également en \unit{\kg\per\l}, \unit{\g\per\mL}, \unit{\g\per\centi\m\cubed}\dots
  \formule{$ \gamma_{\text{i}} = \dfrac{m_\text{i}}{V_{\text{sol}}}
    $}{$ \gamma_{\text{i}} $ :
    \unit{\conc}\\
    $ m_\text{i} $ : \unit{g}\\
    $ V_{\text{sol}} $ : \unit{L} }
\end{aConnaitre}
%
\begin{minipage}{.6\linewidth}
  \begin{ex}{Le Coca-Cola}
    La concentration de sucre dans le Coca-Cola est d'environ \qty{106}{\conc}.
    
    On peut en déduire la masse de sucre dans une canette: 
    \begin{align*}
      m_\text{sucre} &= \gamma_\text{sucre} \times V_\text{coca}\\
        &= 106 \times \num{33e-2}\\
        &= \qty{35}{g}
    \end{align*}
  \end{ex}
\end{minipage}
\begin{minipage}{.3\linewidth}
  \centering
  \includegraphics[scale=.5]{img/coca_sucre}
\end{minipage}
%----------------------------------
\section{Préparation d'une solution.}
\subsection{Préparation d'une solution par dissolution}
\textbf{Matériel nécessaire:} fiole jaugée du volume adéquat, balance, coupelle de pesée,
entonnoir, pipette compte-goutte.

\textbf{La fiole jaugée est indispensable} afin de connaitre le volume, et donc la
concentration de la solution préparée précisément.

\begin{grandCadre} \textbf{Protocole}
  \begin{dingautolist}{202}
    \item Peser une masse $m$ de solide et l'introduire dans une fiole jaugée
    de volume $V$.
    \item Ajouter de l'eau distillée jusqu'à mi-hauteur et agiter jusqu'à
    \textbf{dissolution totale}.
    \item Compléter avec de l'eau distillée jusqu'au trait de jauge et agiter
    pour homogénéiser.
  \end{dingautolist}
\end{grandCadre}
%
\begin{figure}[ht]	
  \begin{minipage}{.74\linewidth}
    \centering
    \includegraphics[scale=.3]{./img/dissolution}
    \caption{Principales étapes d'une dissolution}
  \end{minipage}
  \begin{minipage}{.25\linewidth}
    \centering
    \qrcode{https://colibris.link/dissolutionConcentration}

    {\small\url{colibris.link/dissolutionConcentration}}
  \end{minipage}
\end{figure}
La dissolution d'un soluté dans un solvant a une limite qui dépend de la nature du soluté,
du solvant et de la température. On dit qu'une solution est \textbf{saturée} lorsqu'on ne
peut plus dissoudre de soluté.
%
\begin{ex}{}
  La concentration du sérum physiologique est $\gamma = \qty{9}{\conc}$. Pour en préparer un
  volume $V = \qty{20.0}{\mL}$, la masse à peser est :
  \[
    m = \gamma \times V =  9 \times \num{20e-3} = \qty{0.18}{\g}
  \] 
\end{ex}
%---------------------
\subsection{Préparation d'une solution par dilution}
\textbf{Matériel nécessiare:} fiole et pipette jaugées, pipette compte-goutte.
\begin{grandCadre}
  \begin{dingautolist}{202}
    \item Prélever un volume $V_{\text{m}}$ de la solution à diluer à l'aide d'une
    \textbf{pipette jaugée} et le verser dans une \textbf{fiole jaugée}.
    \item Compléter jusqu'au trait de jauge avec de l'eau distillée et agiter pour
    homogénéiser.
  \end{dingautolist}
\end{grandCadre}
%
\begin{figure}[ht]
  \centering
  \begin{minipage}{.7\linewidth}
    \includegraphics[scale=.1]{./img/dilution}
    \caption{Principales étapes d'une dilution}
  \end{minipage}
  \begin{minipage}{.25\linewidth}
    \centering
    \qrcode{https://colibris.link/dilutionMasse}

    {\small\url{colibris.link/dilutionMasse}}
  \end{minipage}
\end{figure}
%
\setlength{\columnseprule}{2pt}
\renewcommand{\columnseprulecolor}{\color{lightgray}}
\begin{multicols}{2}
  \subsubsection*{\centering Conservation de la masse}
  Lors d'une dissolution, la masse de soluté dans le volume de solution mère prélevé est
  égale à la masse de soluté dans la solution fille.
\begin{center}
  \cadre{\color{red}$ \symbfit{\gamma_{\text{m}} \times V_{\text{m}} = \gamma_{\text{f}} \times V_{\text{f}} }$}
\end{center}
\subsubsection*{\centering Le facteur de dilution}
Le facteur de dilution est défini comme \textbf{le rapport de la concentration de la
  solution mère sur celle de la solution fille}.

De plus, on montre que: $ \frac{\gamma_\text{m}}{\gamma_\text{f}} = \frac{V_\text{f}}{V_\text{m}} $
\begin{center}
  \cadre{\color{red}$\symbfit {f} =
    \dfrac{\symbfit{\gamma_\text{m}}}{\symbfit{\gamma_\text{f}}} =
    \dfrac{\symbfit{V_\text{f}}}{\symbfit{V_\text{m}}}$}
\end{center}
\end{multicols}
\begin{demo}
  La conservation de la masse de soluté s'écrit: 
  \[ m_\text{m} = m_\text{f} \]
  Or, 
  \[ m_\text{m} = \gamma_\text{m} \times V_\text{m} \]
  Et, 
  \[ m_\text{f} = \gamma_\text{f} \times V_\text{f} \]
  On remplace  $m_\text{m}$ et $m_\text{f}$ par leur expression et on obtient: 
  \[ \gamma_{\text{m}} \times V_{\text{m}} = \gamma_{\text{f}} \times V_{\text{f}} \]
  Finalement on obtient: 
  \[ \dfrac{\gamma_\text{m}}{\gamma_\text{f}} = \dfrac{V_\text{f}}{V_\text{m}} \]
\end{demo}
%----------------------
\section{Détermination expérimentale de la concentration d'une solution}
De nombreuses propriétés d'une solution comme sa couleur, ou sa masse volumique dépendent
de sa concentration.
\begin{aConnaitre}
  Un \textbf{dosage par étalonnage consiste à déterminer la concentration d'une solution
    inconnue par comparaison avec une gamme de solutions, du même soluté, et de
    concentrations connues.}
\end{aConnaitre}
%
\subsection{Utilisation d'une échelle de teinte}
La couleur d'une solution dépend de la concentration de l'espèce colorée.
\begin{aConnaitre}
  Lorsque le soluté est une espèce colorée, la gamme d'étalonnage est appelée
  \textbf{échelle de teinte}.
\end{aConnaitre}
\begin{ex}{}
  Dans le cas ci-dessous, la concentration de la solution inconnue est comprise en $\gamma_4$
  et $\gamma_5$.
\end{ex}
\begin{figure}[ht]
  \centering \includegraphics[scale=.1]{img/echelle_teinte}
  \caption{Une échelle de teinte}
\end{figure}
%
\clearpage
\subsection{Tracer d'une droite d'étalonnage}
On peut comparer la couleur d'une solution de concentration inconnue avec celles de
l'échelle de teinte et donner un encadrement de sa concentration.
\begin{grandCadre}
  \textsc{protocole d'un dosage par étalonnage}
  \begin{dingautolist}{202}
    \item Préparer une \textbf{gamme d'étalonnage} par dilution d'une solution de
    concentration connue.
    \item Pour chaque solution, mesurer une grandeur physique dépendante de la
    concentration.
    \item Tracer la courbe représentant la grandeur physique en fonction de la
    concentration.
    \item Mesurer cette grandeur pour une solution de concentration inconnue.
    \item Par lecturegraphique, ou en utilisant l'équation de la courbe de tendant,
    déterminer la concentration de cette solution.
  \end{dingautolist}
\end{grandCadre}
%
\newcommand{\ordo}{\fpeval{7.5}}
\newcommand{\absi}{\fpeval{round(\ordo/3, 1)}}
\begin{ex}{Une courbe d'étalonnage}
  avec le tableau de valeurs ci-dessous, on peut tracer la courbe d'étalonnage.  Si la
  grandeur d'intérêt vaut \textcolor{red}{\num{\ordo}} pour une solution, on peut en déduire
  que sa concentration est de \textcolor{green!60!black}{\qty{\absi}{\conc}}
\end{ex}
\begin{minipage}{.3\linewidth}
  \begin{tblr}{
      vlines, hlines,
      columns={wd=2cm}, rows={c},
      row{1}={font=\bfseries, valign=m},
      hline{2} = {1}{-}{solid},
      hline{2} = {2}{-}{solid},
    }
  {Grandeur \\ d'intérêt} & $\symbf{\gamma}$ \\
  0          &   0         \\
  1          &   3         \\
  2          &   6         \\
  3          &   9         \\
  4          &   12        \\
\end{tblr}
\end{minipage}
%
% -------------------------------------------------------------------------------
\begin{minipage}{.69\linewidth}
  \centering
  \begin{tikzpicture}
    \begin{axis}[
      % scale=1.2,
      width  =  \linewidth,
      height = 7.5cm,
      title = Courbe d'étalonnage,
      xlabel = $\gamma$ / \unit{\conc},
      ylabel = Grandeur d'intérêt,
      xmin=0, ymin=0,
      grid = both,
      minor tick num=2,
      tickpos=left,
      scaled ticks=true, % pour ajouter puisssance de 10 global
      tick scale binop=\times, % avec le signe "x"
      minor grid style = {color=lightgray!20},
      major grid style = {color = lightgray},
      legend pos = north west,
      extra y ticks={\ordo}, extra y tick style= {color=red},
      extra x ticks={\absi}, extra x tick style = {color=green!60!black},
      legend entries={$y = 3 \times x$},
      ]
      \addplot[color=blue, mark=*] {3*\x};
      \draw[->, red, ultra thick, dashed] (axis cs:0,\ordo) -| (axis cs:\absi,0);
    \end{axis}
  \end{tikzpicture}
\end{minipage}
\end{document}
