\documentclass[french]{exam}
\input{ ../../../preambules/mes_packages_lua }
\input{ ../../../preambules/style_act }
\header{\textbf{Chapitre C2.} \textcolor{gray!60}{Seconde}}
{}
{\themeMatiere}
%
\DeclareSIUnit{\conc}{\g\per\l}
% \printanswers
%
% -------------------------------------------------------------------------------
%
\begin{document}
\titre{\textbf{AN.} Dosage par étalonnage du Coca-Cola}

\begin{contexte}
  Le contrôle qualité est une étape importante dans l'industrie: il s'agit de vérifier si
  les caractéristiques d'un produit sont conformes à ce qu'anonce le fabriquant.
	
  On souhaite ici vérifier la concentration en masse de sucre dans une canette de
  Coca-cola.
\end{contexte}
%
% -------------------------------------------------------------------------------
%
\section{\infos}
\begin{docs}
  \doc{Étiquette d'une canette de Coca-cola}{etiquette}
  \centering
  \includegraphics[scale=.2]{img/coca_etiquette}
  %
  \doc{Préparation de la  courbe d'étalonnage}{courbe}
  \begin{enumerate}[leftmargin=*, label=\ding{217}]
    \item Préparer la gamme étalon par dilution d'une solution d'eau sucrée de
    concentration connue.
    \item Mesurer la grandeur d'intérêt (ici la masse volumique) pour chaque solution de
    la gamme étalon et le reporter dans un tableau.
    \item Tracer le graphique représentant la masse volumique en fonction de la
    concentration en sucre.
  \end{enumerate}
  %
  \doc{Gamme étalon}{gamme}
  \centering
  \begin{tabular}{ccc}
    Solutions & $\gamma_{\text{m}}$ / \unit{\conc} & $\rho$ / \unit{\conc}\\
    \toprule
    $S_0$ & 200& 1076\\
    $S_1$ & 100& 1036\\
    $S_2$ & 50 & 1016\\
    $S_3$ & 40 & 1012\\
    $S_4$ & 25 & 1006\\
    $S_5$ & 20 & 1004\\
    \bottomrule
  \end{tabular}
  %
\end{docs}
%
% -------------------------------------------------------------------------------
%
\section{\travail}
\begin{questions}
  \question Expliquer pourquoi on peut confondre la masse volumique d'une solution et la concentration d'un soluté. Expliquer la différence entre ces deux grandeurs.
  \begin{solution}
    Ces deux grandeurs ont la même unité. La différence est la masse considérée: pour la
    masse volumique de la solution, on prend la masse de la solution alors que pour la
    concentration d'un soluté, on prend la masse d'un soluté.
  \end{solution}
  %
  \question Écrire le protocole expérimental permettant de préparer \qty{50}{\mL} de
  solution $S_3$ à partir de $S_0$. Le calcul du volume à prélever est attendu.
  \begin{solution}
    \begin{itemize}
      \item \textbf{Détermination du volume de solution $S_0$ à prélever}
      
      Lors d'une dilution, il y a conservation de la masse soluté:
      \[ \gamma_0 \times V_0 = \gamma_3 \times V_3 \]
      Soit
      \begin{align*}
        V_0 &= \dfrac{\gamma_3 \times V_3}{\gamma_0} \\
            &= \dfrac{40 \times 50}{200}\\
            &= \qty{10}{\mL}
      \end{align*}
      \item \textbf{Protocole expérimental:}
      
      On prélève \qty{10}{\mL} de solution $S_0$ à l'aide d'une pipette jaugée et on les
      introduit dans une fiole jaugée de \qty{50}{\mL}.
      
      On complète avec de l'eau distillée jusqu'au trait de jauge et on agite pour homogénéiser.
    \end{itemize}
  \end{solution}
  %
  \question Sur sa calculatrice, tracer la droite d'étalonnage et la courbe de
  tendance. Reproduire l'allure du graphique obtenu et l'équation de la
  modélisation. \refDoc{gamme}
  \begin{solution}    
    \newcommand{\ordo}{\fpeval{1038}}
    \newcommand{\absi}{\fpeval{round((\ordo-996)/.4, 1)}}
    \begin{tikzpicture}
      \begin{axis}[
        title=Droite d'étalonngage,
        xlabel = $\gamma_{\text{m}}$ / \unit{\conc},
        ylabel = $\rho$ / \unit{\conc},
        grid = major,
        xmin = 0,
        extra x ticks={\absi}, extra x tick style= {color=red, grid=none, ticklabel shift=10pt},
        legend entries = {Points expérimentaux, $y=\num{.4} x+996$}
        ]
        \addplot[only marks]coordinates {(200,1076) (100,1036) (50,1016) (40,1012)
          (25,1006) (20,1004) };
        \addplot[samples=10,domain=0:200] {996 + .4*\x};
        \draw[red, ultra thick, dashed] (axis cs:0,\ordo) -|(axis cs:\absi, 0);
      \end{axis}
    \end{tikzpicture}
  \end{solution}
  %
  \question On mesure la masse de \qty{100}{\mL} de Coca-Cola et on trouve \qty{103.8}{\g}.
  \begin{parts}
    \part Déterminer la masse volumique du Coca-Cola.
    \begin{solution}
      \[ \rho = \dfrac{103,8}{0,1} = \qty{1038}{\g\per\l} \]
    \end{solution}
    \part Évaluer graphiquement la valeur de la  concentration en sucre dans le
    Coca-Cola.
    \begin{solution}
      Par lecture graphique, on peut lire qu'une masse volumique de \qty{1035}{\conc}
      correspond à une concentration légèrement supérieure à \qty{100}{\conc}.
    \end{solution}
    \part Utiliser l'équation de modélisation pour calculer la concentration de sucre.
    \begin{solution}
      En utilisant l'équation de la courbe de tendance, on peut écrire: 
      \begin{align*}
        \gamma_{\text{m}} &= \dfrac{\rho - 996}{0,4} \\
            &= \dfrac{1038 - 996}{0,4} \\
            &= \qty{105}{\conc} 
      \end{align*}
    \end{solution}
    \part Conclure
    \begin{solution}
      Les valeurs des deux questions précédentes sont cohérentes et on trouves des
      résultats qui correspondent aux indication sur l'étiquette.
    \end{solution}
  \end{parts}
  \question Calculer la masse de sucre que l'on ingère lorsqu'on boit une canette de
  Coca-cola. En déduire le nombre de morceau de sucre de \qty{6}{\g} correspondant.
  \refDoc{etiquette}
\end{questions}
\end{document}
