\documentclass[french]{article}

\input{ ../../../preambules/mes_packages_lua.tex }
\input{ ../../../preambules/style_cours.tex }
\fancyhead[L]{\textbf{Chapitre O4.} \textcolor{gray!70}{Seconde}}
\fancyhead[R]{\themeOndes}
% -------------------------------------------------------------------------------
\begin{document}
\titre{Le son}
% -------------------------------------------------------------------------------
\section{Propagation d'un son}
\begin{aConnaitre}
  \begin{itemize}
    \item Le son se propage de proche en proche par mise en vibration successive de la
    matière : un signal sonore ne peut se propager que dans un \textbf{milieu matériel}
    (air, métal …).
    \item La \textbf{vitesse de propagation} d’un signal sonore dépend du milieu de
    propagation et de ses caractéristiques (masse volumique, température, pression…).
  \end{itemize}
%
  \centering \cadre{\textbf{À retenir : $ v_{air} = \qty{340}{\m\per\s}$ et
      $ v_{eau} = \qty{1 500}{\m\per\s}$} }
\end{aConnaitre}

\begin{minipage}{0.7\linewidth}
  \begin{ex}{Dans le vide}
    Une explosion dans le vide ne ferait donc aucun bruit, \newline contrairement à ce
    qu’on entend au cinéma.
  \end{ex}
\end{minipage}	
\begin{minipage}{0.3\linewidth}
  \includegraphics[scale=.35]{./img/explosion}
\end{minipage}

\textbf{Caractéristiques du milieu de propagation :}
%
\begin{center}
\begin{tabular}{|c|c|}
  \hline  
  État physique &   Température \\ 
  \hline
  $ v_{gaz}  <  v_{liquide}  <  v_{solide} $ &  La vitesse du son dans l'air varie selon la température.
  \\\hline
\end{tabular}
\end{center}
\bigskip
\begin{center}
Variation de v en fonction de T 

\includegraphics[scale=.45]{./img/graph_v(T)}
\end{center}
%
{\textbf{Ordre de grandeur de vitesses}}

\begin{tabular}{|>{\centering\arraybackslash}m{2.5cm}|*{7}{>{\centering\arraybackslash}m{1.3cm}|}}
  \hline
  \rowcolor{gray!40}
  & marcheur
  & vélo
  & voiture
  & train
  & avion
  & son
  & lumière
  \\\hline
  $ v $ / \si{\km\per\hour}
  &
  &
  & 80
  & 300
  & 900
  &
  &  
  \\\hline
  $ v $ / \si{\m\per\s}
  & 1,4
  & 5,6
  &
  &
  &
  &
  &
  \\\hline
\end{tabular}
%
\begin{multicols}{2}
  \begin{ptMath}{}
    \[
      \frac{km}{h}=\frac{\qty{1e3}{m}}{\qty{3600}{s}}=\frac{1}{3,6}\times\frac{m}{s}
    \]
  \end{ptMath}
\end{multicols}
\section{Période, fréquence et hauteur}
\subsection{Visualisation d'un signal périodique}
%
\begin{aConnaitre}
  Un microphone permet de transformer un signal sonore en un signal électrique que l’on
  peut visualiser.
  \begin{itemize}
    \item Un signal est \textbf{périodique} s’il \textbf{se reproduit identique à
      lui-même} à intervalle de temps égaux.
    \item Le \textbf{motif élémentaire} est la plus petite partie de la courbe qui se
    répète.
  \end{itemize}
\end{aConnaitre}
%
% \begin{minipage}{0.15\linewidth}
\begin{ex}[sidebyside, lefthand ratio = .7]{}
  \centering
  \includegraphics[scale=.4]{./img/periodique_non_periodique}
  \tcblower
  \centering
  \qrcode[height=1cm]{https://www.youtube.com/watch?v=DI5ioNtI2uI}
\end{ex}
% \end{minipage}	
% \hspace{0.1cm}
% \begin{minipage}{0.6\linewidth}
% \end{minipage}
%
\subsection{Fréquence, période}
%
\begin{aConnaitre}
  Lorsqu'un signal est périodique, on peut définir :
  \begin{itemize}
    \item sa \textbf{période T} (exprimée \textbf{en seconde}) qui correspond à la plus
    petite durée au bout de laquelle un signal sonore se reproduit.
    \item sa \textbf{fréquence f} (exprimée \textbf{en hertz}) qui correspond à un nombre
    de motifs par seconde.
  \end{itemize}
  L’amplitude d’un signal est la valeur maximale atteinte.
\end{aConnaitre}
%
\noindent\textbf{La fréquence et la période sont liées par la relation suivante :}

\begin{center}
  \begin{minipage}{0.85\linewidth}
    \formule{$ f = \dfrac{1}{T}$ }{
      $f$ en \si{Hz} \\
      $T$ en \si{s} }
  \end{minipage}
  % 
  \begin{minipage}{.1\linewidth}
    \qrcode{https://www.youtube.com/watch?v=OJR34PqWw4U}
  \end{minipage}
\end{center}
% \includegraphics[scale=.3]{signal_cours}
\textbf{Domaines de fréquences}
\begin{aConnaitre}
  \begin{minipage}{0.6\linewidth}
    Un son est un signal sonore audible dont la fréquence est comprise entre 20 Hz et 20
    kHz.
  \end{minipage}
  \hspace{0.1cm}
  \begin{minipage}{0.5\linewidth}
    \includegraphics[scale=.3]{./img/gamme_frequence.png}
  \end{minipage}
\end{aConnaitre}

\begin{minipage}{.7\linewidth}
  \begin{ex}{}
    Les systèmes auditifs de certains animaux leur permettent d’entendre des sons que les
    humains ne peuvent percevoir. les chiens entendent des sons de fréquences supérieures
    à 20 000 Hz.
  \end{ex}
\end{minipage}
\begin{minipage}{.25\linewidth}
\begin{center}
  \includegraphics[scale=.2]{./img/chien}
\end{center}
\end{minipage}
\subsection{Hauteur d'un son}
\begin{aConnaitre}
  \begin{itemize}
    \item La hauteur est la caractéristique d’un son qui est reliée à sa fréquence : plus
    sa fréquence est élevée, plus un son est aigu. Inversement, plus sa fréquence est
    faible, plus un son est grave.
    \item Dans le langage courant, un son aigu est souvent qualifié de « haut ».
  \end{itemize}
\end{aConnaitre}
%
\begin{multicols}{2}
  \begin{center}
    \includegraphics[scale=.25]{./img/hauteurSon}
  \end{center}
  \columnbreak Pour jouer un $la_3$, une corde de guitare doit réaliser 440 vibrations par
  seconde qui vont ensuite se propager dans l’air. \textit{ On note la fréquence :
    $ f_{la} = \qty{440}{Hz} $}
\end{multicols}
% -------------------------------------------------------------------------------
\section{Timbre d'un son}
\begin{aConnaitre}
  Le timbre est ce qui \textbf{différencie deux sons de même hauteur et de même niveau
    sonore}. Ainsi deux instruments de musique, les voix de deux personnes différentes
  peuvent être distingués les uns des autres car leur timbre est différent.
\end{aConnaitre}

\begin{ex}{}
  Sur les enregistrements de deux instruments jouant la même note, la période est la même
  mais la forme du motif est différente, témoignant de la différence de timbre.
\end{ex}
\begin{center}
  \includegraphics[scale=.38]{./img/timbre}
\end{center}
% -------------------------------------------------------------------------------
\section{Amplitude, intensité sonore et niveau sonore}
\begin{aConnaitre}
  \begin{itemize}
    \item L’\textbf{amplitude} d’un signal correspond en général à sa \textbf{valeur
      maximale}. Plus l'amplitude d'un son est élevé, plus l'\textbf{intensité sonore I}
    est grande (I exprimée en $ W.m^{-2}$).
    \item Le \textbf{niveau d’intensité sonore L}, exprimé en \textbf{décibels dB}, permet
    de comparer les sons. Sa valeur varie de 0 pour le seuil d’audibilité à 120 pour le
    seuil de douleur.
    \item Le niveau sonore L se mesure à l'aide d'un \textbf{sonomètre}.
  \end{itemize}
\end{aConnaitre}

\begin{minipage}{.35\linewidth}
  \begin{center}
    \includegraphics[scale=.35]{./img/LetI}
  \end{center}
\end{minipage}
\begin{minipage}{.6\linewidth}
  \attention{ \textbf{Il ne faut pas confondre I et L} : ces deux grandeurs varient dans
    le même sens mais elles ne sont pas proportionnelles.  }
\end{minipage}
%
\subsection{Dangers inhérents à l'exposition sonore}
\textbf{Plus un son est intense, plus la durée d’exposition doit être courte}. Les sons
les plus intenses sont très dangereux dès la première seconde.  Les lésions causées aux
oreilles sont \textbf{irréversibles}.

Les sons présentent des risques dès 85 dB et des dommages surviennent à partir de 90 dB
alors que la sensation ne devient réellement douloureuse que vers 120
dB. \textbf{L’absence de douleur ne garantit donc pas l’absence de danger !} Il est
nécessaire d’être vigilant et \textbf{de se protéger} des sons trop intenses.

\begin{center}
  \includegraphics[scale=.35]{./img/echelle_L}
\end{center}
% -------------------------------------------------------------------------------
\section{Produire un son}
\begin{aConnaitre}
  \begin{itemize}
    \item Un \textbf{signal sonore} est émis par la mise en vibration d’un objet. Cette
    vibration est transmise à l’air qui l'entoure.
    \item La \textbf{caisse de résonance} favorise la transmission du son dans l'air.
  \end{itemize}
\end{aConnaitre}

\begin{minipage}{0.7\linewidth}
  \begin{ex}{La guitare}
    Une corde de guitare qui vibre produit un son. \newline Le signal sonore est amplifié
    dans la caisse de résonance en bois.
  \end{ex}
\end{minipage}	
\begin{minipage}{0.35\linewidth}
  \includegraphics[scale=.3]{./img/guitare}
\end{minipage}
\end{document}
