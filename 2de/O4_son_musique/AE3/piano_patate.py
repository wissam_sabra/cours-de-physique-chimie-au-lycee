### Initialisation ###
from microbit import *
import music

# On éteint l’écran pour pouvoir utiliser le pin3
display.off()

# Réglage des fréquences
freq1 = 524
freq2 = 588
freq3 = 660
limite = 60

touche1 = pin1
touche2 = pin2
touche3 = pin3

### Boucle infinie ###
while True :
    if touche1.read_analog >limite :
        music.pitch(freq1, 300)
    if touche2.read_analog >limite :
        music.pitch(freq2, 300)
    if touche3.read_analog >limite :
        music.pitch(freq3, 300)
    sleep(5)

    # Pour vérification éventuelle
    print(touche1)
    print(touche2)
    print(touche3)
    sleep(1000)