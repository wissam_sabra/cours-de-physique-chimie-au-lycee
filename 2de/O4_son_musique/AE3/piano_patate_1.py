''' Le piano patate. avec microbit v1 / shiel elecfreaks avec buzzer intégré / résistance 1MOhm '''
### Initialisation ###
from microbit import *
import music

# On éteint l’écran pour pouvoir utiliser le pin3
display.off()

# Réglage des fréquences
freq1 = 524
freq2 = 588
freq3 = 660
limite = 60


limite=500
### Boucle infinie ###
while True:
    touche1 = pin1.read_analog()
    touche2 = pin2.read_analog()
    touche3 = pin3.read_analog()
    if touche1 >limite :
        music.pitch(freq1, 300)
    if touche2 >limite :
        music.pitch(freq2, 300)
    if touche3 >limite :
        music.pitch(freq3, 300)
    print(touche1, touche2, touche3)
    sleep(10)