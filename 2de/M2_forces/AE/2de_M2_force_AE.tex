\documentclass[french]{exam}
\input{../../../preambules/mes_packages_lua}
\input{../../../preambules/style_act}
\header{\textbf{Chapitre M2.} \textcolor{gray!60}{Seconde}}
{}
{\themeMvt}
% -------------------------------------------------------------------------------
\begin{document}
\titre{\textbf{AE.} Le principe d'inertie}
%
\begin{contexte}
  \textbf{Objectif :} Comprendre le lien entre forces et mouvement au travers 
  du principe d'inertie.
\end{contexte}
%
\section{\infos}
\begin{docs}
  \docL[sidebyside, lefthand ratio=.65]{Le curling}{curling}
  Le curling est un sport de précision pratiqué sur la glace avec des 
  « pierres » en granite de \qty{20}{\kg} taillée et polie selon un gabarit
  internationnal.
  
  Le but est de lancer la pierre le plus près possible d'une cible circulaire
  dessinée sur la glace.
  
  Le rôle des balayeurs est de diminuer au maximum les frottement de la glace
  sur la pierre.
  \tcblower
  \centering
  \includegraphics[scale=.5]{./img/curling}
  % 
  \docL[sidebyside, lefthand ratio=.65]{Le mobile autoporteur}{mobile}

  Le mobile autoporteur, aussi appelé mobile sur coussin d’air, 
  est un appareil muni d’un dispositif qui lui permet de 
  \emph{glisser sans frottement sur une surface plane.}
  Il dispose d’un système de jet d’encre permettant 
  de marquer sa position au cours d’un mouvement. 
  La télécommande permet de déclencher le jet d’encre et de 
  régler la durée $ \Delta t $ entre deux points successifs.
  \tcblower
  \centering
  \includegraphics[scale=.2]{./img/mobile}
  % 
  \docL{Simulation du mouvement de la pierre}{pointage}
  On réalise une simulation du mouvement de la pierre au cours d'un lancé et
  on présente le pointage correspondant ci-dessous.

  \centering
  \includegraphics[width=\linewidth]{./img/simul}
  % 
  \doc{Réaction d'un support et frottement}{reaction}
  L'action d'un support peut être modélisée par deux forces : 
  \begin{itemize}
    \item \emph{La réaction normale} $\vv{R_N}$ perpendiculaire à la 
    surface et orientée vers le haut, elle a la même valeur que le 
    poids du corps posé sur sur support.
    \item \emph{La force de frottement} $ \vv{f} $, toujours \emph{dans 
      le sens opposé au mouvement}.
  \end{itemize}
  
  \centering
  \begin{tikzpicture}
    \draw[->, thick, dashed] (-1,0) -- (5,0) node[near end, above] 
    {Sens du mouvement};
    \coordinate (M) at (1,0);
    \draw (M) node {$ \bullet $} node[below right] {$ M $};
    \draw[blue!80!black, ->] (M) -- (1,2) 
    node[midway, right] {$ \vv{R_N} $};
    \draw[blue!80!black, ->] (M) -- (1,-2) 
    node[midway, left] {$ \vv{P} $};
    \draw[blue!80!black, ->] (M) -- (0,0) 
    node[midway, above] {$ \vv{f} $};
  \end{tikzpicture}
  \doc{Le principe d'inertie}{principeInertie}
  Aussi appelé la première loi de Newton, on l'énonce ainsi:	
  \textbf{\textcolor{red}{si les forces qui s'exercent sur un corps se compensent, alors
  il est immobile ou en mouvement rectiligne uniforme et 
  réciproquement.}}
\end{docs}
% -------------------------------------------------------------------------------
\section{\travail}
\subsection{Questions préliminaires}
\begin{enumerate}
  \item Avant le lancé, la pierre est immobile, posée sur la glace:
  \begin{enumerate}
    \item Faire le DOI de cette situation.
    \item Identifier les deux forces auxquelles est soumise la pierre et 
    préciser leur caractéristiques (sens, direction, valeur)
    \item En représentant la pierre par un point matériel, représenter les 
    forces auxquelles elle est soumise à l'échelle \SI{1}{cm} pour 
    \SI{100}{\newton}
    \item Que peut-on dire de ces forces?
  \end{enumerate}
  \item Identifier les trois phases du mouvement de la pierre en caractérisant
  le mouvement correspondant.
\end{enumerate}
% 
\subsection{Partie 1 : étude de la phase 2 }
\begin{itshape}
  Pour vérifier la nature du mouvement dans cette phase, on utilise un mobile
  autoporteur. Le marquage des ses positions successives commence \emph{à 
    l'instant où il n'est plus en contact avec la main}.
\end{itshape}
\begin{enumerate}[resume]
  \item Après avoir effectué le marquage des positions du mobile autoporteur:
  \begin{enumerate}
    \item Exprimer et calculer la vitesse en deux points différents de la 
    trajectoire. Préciser les points choisis.
    \item Tracer les vecteurs correspondant en précisant l'échelle choisie.
    \item Caractériser le mouvement.
  \end{enumerate}
  \item Réaliser le DOI de cette situation.
  \item En déduire les forces qui s'appliquent sur le mobile.
  \item Expliquer en quoi cette interprétation est contre-intuitive.
\end{enumerate}
%
\vspace{.5cm}
\textbf{Données:} 

L'intensité de pesanteur sur Terre vaut 
$ g = \SI{9.8}{\N\per\kg} $
% 
\clearpage
\subsection{Partie 2 : étude des trois phases du lancé}
Compléter le tableau ci-dessous
\begin{table}[ht]
  \centering
  \begin{tabular}{
    |>{\centering\arraybackslash}m{2.5cm}
    |>{\centering\arraybackslash}m{3.8cm}
    |>{\centering\arraybackslash}m{3.8cm}
    |>{\centering\arraybackslash}m{3.8cm}|
    }
    \hline
    \rowcolor{yellow!20}
    & \textbf{Phase 1}
    & \textbf{Phase 2}
    & \textbf{Phase 3}
    \\\hline
    DOI de la situation
    &\rule{0cm}{5cm}
    &
    &
    \\\hline
    Représentation des vecteurs forces (sans tenir compte de l'échelle)
    & \rule{0pt}{5cm}
    &
    &
    \\\hline
    \[\sum \vv{F} = \vv{0} \] ou \[ \sum \vv{F} \neq \vv{0} \]
    & \dotfill 
    & \dotfill 
    & \dotfill 
    \\\hline
    Variation du vecteur vitesse? Si oui préciser la caractéristique 
    qui varie.
    & \dotfill 
    & \dotfill  
    & \dotfill  
    \\\hline
    Caractériser le mouvement de la pierre
    & \dotfill 
    & \dotfill 
    & \dotfill 
    \\\hline
    
  \end{tabular}
\end{table}
%
\clearpage
\subsection{Partie 3 : Application au mouvement de la Terre}
Dans le référentiel géocentrique, on peut considérer que la Terre est en 
mouvement \emph{circulaire uniforme}.
% \clearpage
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[scale=1]
    \newcommand{\rayon}{2}
    \coordinate (S) at (0,0) node[left] {S} node {$ \bullet $} ;
    \draw (0:\rayon) node[left] {$ T_0 $};
    \draw (45:\rayon) node[below left] {$ T_3 $};
    \draw (90:\rayon) node[below] {$ T_6 $};
    \draw (180:\rayon) node[right] {$ T_{12} $};
    \draw (-90:\rayon) node[above] {$ T_{18} $};
    \foreach \a in {0,15,30,...,345}
    {\draw[blue] (\a:\rayon) node {$ \bullet $} ;};
    \draw[->, red, thick](2,0) --(2,2)  node[midway, right] {$ \vv{v_0} $};
    \draw[->, red, thick](45: \rayon)--++(135:2)  
    node[midway, right] {$ \vv{v_3} $};
    \draw[->, red, thick](90: \rayon)--++(180:2)  
    node[midway, above] {$ \vv{v_6} $};
    \draw[->, red, thick] (4,0) -- (6,0) 
    node[midway, above]{\SI{3e4}{\m\per\second}};
    \node (E) at (5,1) {Échelle:};
  \end{tikzpicture}
\end{figure}

\begin{enumerate}[resume]
  \item Vérifier que la figure ci-dessus correspond à un mouvement
  \emph{uniforme}.
  \item Tracer les vecteurs vitesses $ \vv{v_{12}} $ et $ \vv{v_{18}} $.
  \item Les forces qui s'exercent sur la Terre se compensent-elle? Justifier
  la réponse.
\end{enumerate}
% 
\end{document}
